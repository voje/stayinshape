#include <stdio.h>
#include <string.h>

//allow stack overflow:
//gcc -fno-stack-protector

void fun(char* arg1) {
	int access = 0;
	char a[5];
	strcpy(a, arg1);
	if (access) {
		printf("Access approved!\n");
	} else {
		printf("Access denied!\n");
	}	
}

int main() {		
	//overflows the 5 buffer and writes into stack space of access. (this is the minimum number of extra chars that worked.
	char* arr = "Test!12345678"; 
	fun(arr);
	return 0;
}
