#!/usr/bin/python3
import json
import pickle
import random

# Creates usable [1,2,3,4]-word lists 
# in .json and .pickle format.

one = []
with open("krizanke.txt", "r") as f:
    for line in f:
        one.append(line)

one = list(set([x.strip() for x in one]))

def concat(x, y):
    return " ".join([x, y])

two = list(map(concat, one, reversed(one)))

three = list(map(concat, two, sorted(one)))

four = list((map(concat, three, random.sample(one, len(one)))))

data = {
        "one": one,
        "two": two,
        "three": three,
        "four": four,
}

for k, e in data.items():
    print(k)
    print(e[:10])

with open("krizanke.json", "w") as f:
    json.dump(data, f)

with open("krizanke.pickle", "wb") as f:
    pickle.dump(data, f)


# sample of 100 entries

data100 = {
    "one": random.sample(one, 100),         
    "two": random.sample(two, 100),         
    "three": random.sample(three, 100),         
    "four": random.sample(four, 100),         
}

with open("krizanke100.json", "w") as f:
    json.dump(data100, f)

with open("krizanke100.pickle", "wb") as f:
    pickle.dump(data100, f)
