use std::io;

fn c_to_f(f: f64) -> f64 {
	f / 5.0 * 9.0 + 32.0
}

fn f_to_c(c: f64) -> f64 {
	(c - 32.0) / 9.0 * 5.0
}

fn main() {
	println!("Input temperature with suffix C (celsius) or F(fahrenheit):");

	let mut input = String::new();
	io::stdin().read_line(&mut input)
		.expect("Failed to rnad line.");

	let mut input = String::from(input.trim());

	let suffix: char = input.pop()
	.expect("Couldn't read character.");

	let celsius: f64;
	let fahrenheit: f64;

	match suffix {
		'C' => {
			celsius = input.parse().unwrap();
			fahrenheit = c_to_f(celsius);
		},
		'F' => {
			fahrenheit = input.parse().unwrap();
			celsius = f_to_c(input.parse().unwrap());
		},
		_ => {
			println!("Unknown suffix.");
			return;
		},
	};

	println!("{}C == {}F", celsius, fahrenheit);
}
