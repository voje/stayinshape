use std::collections::HashMap;

fn mean(vc: &Vec<i32>) -> f64 {
    let mut sum = 0;
    for v in vc {
        sum += v;
    }
    (sum as f64) / (vc.len() as f64)
}

// The value that occurs the most.
fn mode(vc: &Vec<i32>) -> i32 {
    let mut m: HashMap<i32, i32> = HashMap::new();
    for v in vc {
        let e = m.entry(*v).or_insert(0);
        *e += 1;
    }
    let mut val_key = (0, -1);
    for (k, e) in m {
        println!("{}, {}", e, k);
        if e > val_key.0 {
            val_key.0 = e;
            val_key.1 = k;
        }
    }
    val_key.1
}

fn median(vc: &Vec<i32>) -> i32 {
    let mut vcp = vc.clone();
    println!("{:?}", vcp);
    insertion_sort(&mut vcp[..]);
    println!("{:?}", vcp);
    let mid = (vc.len() / 2) as usize;
    vcp[mid]
}

fn insertion_sort(lst: &mut [i32]) {
    for i in 1..lst.len() {
        let mut j = i - 1;
        loop {
            if lst[j] <= lst[j + 1] {
                break;
            }
            let tmp = lst[j+1];
            lst[j+1] = lst[j];
            lst[j] = tmp;

            // Break condition... this is a bit more verbose as C.
            // Rust is very strict about usize arithmetic (<0 not allowed).
            if j == 0 {
                break;
            }
            j -= 1;
        }
    } 
}

fn analyze(vc: &Vec<i32>, name: &str) {
    println!("Analyzing vector: {}", name);
    println!("The mean is: {}", mean(&vc));
    println!("The mode is: {}", mode(&vc));
    println!("The median is: {}", median(&vc));
}

fn main() {
    let v1: Vec<i32> = vec![1,2,3,4,5,6];
    analyze(&v1, "v1");

    let v2: Vec<i32> = vec![1,2,65,34,99,65,344,33,33,231,1,2,42,534,3,42,42,326262,4,5555,42,99,0,42,5522];
    analyze(&v2, "v2");
}
