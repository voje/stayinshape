use std::io;
fn fibo(n: u64) -> u64 {
	if n == 0 { return 0 };
	if n == 1 { return 1 };
	let mut r1: u64 = 0;
	let mut r2: u64 = 1;
	for _ in 1..n {
		let tmp = r2;
		r2 = r1 + r2;
		r1 = tmp;
	}
	return r2;
}

fn main() {
	println!("Calculate the n-th fibonacci number:");
	let mut input = String::new();
	io::stdin().read_line(&mut input)
		.expect("Input error.");

	println!("The {}th fibonacci number is: {}",
		input.trim(), fibo(input.trim().parse().unwrap()));
}
