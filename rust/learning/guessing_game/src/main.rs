use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Guess the secret number between including 1 and 100.");

    let secret = rand::thread_rng().gen_range(1, 101);

    // println!("The secret number is: {}.", secret);

    loop {
    	let mut guess = String::new();

	    io::stdin().read_line(&mut guess)
	    	.expect("Failed to read line.");

	    let guess: u32 = match guess.trim().parse() {
			Ok(num) => num,
			Err(_) => {
				println!("Please, enter a number!");
				continue;
			},
	    };

	  	match guess.cmp(&secret) {
	  		Ordering::Less => println!("Too small!"),
	  		Ordering::Equal => {
	  			println!("You've guessed {}. Correct!", guess);
	  			break;
	  		},
	  		Ordering::Greater => println!("Too large!"),
	  	};
	}
}
