fn test(_ai: i64) {}

fn test1(_ai: String) {}

fn main() {
	let a = 42;
	test(a);
	let b = a;
	println!("{}", b);

	let s = String::from("test");
	test1(s);  // s loses ownership (see function signature)
	// let t = s;  // error, s is out of scope by now

	let mut ss = String::from("Left | ");
	// ss.push_str("right");
	let sa = "right";
	ss.push_str(sa);
	println!("{},  {}", ss, sa);

	// String slices
	let _a = "čiv";
	// let _b = &_a[0..1];  // RUNTIME ERROR !!!
}