enum Class {
	Rogue,
	Warrior,
}

fn main() {
	let a = Class::Rogue;
	match a {
		Class::Rogue => println!("Stabby stabby!"),
		Class::Warrior => println!("Shileds up!"),
	}
}
