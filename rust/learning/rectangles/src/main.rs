#[derive(Debug)]
struct Rectangle {
	width: u32,
	height: u32,
}

impl Rectangle {
	fn new(w: u32, h: u32) -> Rectangle {
		Rectangle {width: w, height: h}
	}
	fn area(&self) -> u32 {
		self.width * self.height
	}
}

fn main() {
	let r = Rectangle::new(10, 15);
	println!("Area of rectangle {:?} is: {}", r, r.area());
}
