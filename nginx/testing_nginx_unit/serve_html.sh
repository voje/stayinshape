#!/bin/bash

mkdir -p /www/data/static
echo "Serving html" > /www/data/static/index.html

cat >>config.json <<EOF
{
    "listeners": {
        "127.0.0.1:8300": {
            "pass": "routes"
        }
    },

    "routes": [
        {
            "action": {
                "share": "/www/data/static/"
            }
        }
    ]
}
EOF

curl -X PUT --data-binary @config.json \
--unix-socket var/run/control.unit.sock http://127.0.0.1/config/listeners/127.0.0.1:8300

rm config.json