#!/bin/bash

docker rm -f silly_nginx
docker run -d \
    --name silly_nginx \
    -p 8080:80 \
    -v $PWD:/www \
    -w /www \
    nginx/unit

docker exec -it silly_nginx /bin/bash
