# Testing Nginx Unit
Note: unfinished setup;
TLDR: Use nginx as a proxy server and static server, use Unit for running Apps behind the proxy.   

Nginx Unit is an Application server, although it's still capable of serving static content.   

All configuration is done via the server's REST API.   
Updates without downtime.   

## Playing around
Run the container then execute all commands from inside the container.   
```bash
./run_container.sh
```

Serve a simple html file.   
```bash
./serve_html.sh
```

## Socket
Inside the docker container, we see the 
`/var/run/control.unit.sock`.   

We can either mount the socket to host or (send directly over http, without socket???).   
