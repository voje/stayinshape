# Testing some nginx configurations
Run the nginx container
```bash
./run_nginx.sh
```

Edit (mounted) configuration `nginx.conf` and run the script to apply the configuration to the server in the docker container.   
```bash
./reload_conf.sh
```

## Ports
Have not tested port mapping yet, use `-p n:n` for now.   
