#!/bin/bash

docker exec -it nginx_server bash -c 'cp /workdir/nginx.conf /etc/nginx/nginx.conf; nginx -T; nginx -s reload'
