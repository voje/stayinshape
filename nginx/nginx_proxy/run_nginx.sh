#!/bin/bash

docker rm -f nginx_server
docker run \
    -d \
    --name nginx_server \
    -p 8181:8181 \
    -v $PWD:/workdir \
    nginx