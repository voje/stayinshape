import tensorflow as tf
from tensorflow import keras

import numpy as np
import matplotlib.pyplot as plt

from keras.datasets import mnist

from time import time

(x_train, y_train), (x_test, y_test) = mnist.load_data()
print("x_train dimensions: {}".format(x_train.shape))

"""
plt.figure()
plt.imshow(x_train[0])
plt.colorbar()
plt.show()
"""

# we need values between 0 and 1
x_train = x_train / 255.0
x_test = x_test / 255.0

# Check input data.
plt.figure(figsize=(10, 10))
for i in range(25):
    plt.subplot(5, 5, i + 1)
    plt.imshow(x_train[i])
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.xlabel(y_train[i])
# plt.show()

model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax),
])

model.compile(
    optimizer=tf.train.AdamOptimizer(),
    loss="sparse_categorical_crossentropy",
    metrics=["accuracy"],
)

model.fit(x_train, y_train, epochs=5)

test_loss, test_acc = model.evaluate(x_test, y_test)

print("Evaluation:")
print(test_loss)
print(test_acc)

tstart = time()
predictions = model.predict(x_test)
print(np.argmax(predictions[0]))
print(y_test[0])
