# Using go modules
As of go 1.13, you can use go modules to run and compile go code outside of your $GOPATH. 

Init:
```bash
go mod init  # takes git url as name if you're in a git repository
go mod init <module name>  # for example github.com/voje/gotest
```

For more info: `$ go mod`.  
