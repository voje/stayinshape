package main

import (
  "fmt"
  "html/template"
  "io/ioutil"
  "net/http"
  "regexp"
  //"errors"
)

//cache all the templates
var templates = template.Must(template.ParseFiles("./templates/view.html", "./templates/edit.html"))

//path regex
var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9_]+)$")

type Page struct {
  Title string //public members are upper case
  Body []byte
}

func (p *Page) save() error {
  filename := "./pages/" + p.Title + ".txt"
  return ioutil.WriteFile(filename, p.Body, 0600) //returns nil if all is well
}

func loadPage(title string) (*Page, error) {
  filename := "./pages/" + title + ".txt"
  body, err := ioutil.ReadFile(filename)
  if err != nil {
    return nil, err
  }
  return &Page{Title: title, Body: body}, nil
}

func renderTemplate(name string, w *http.ResponseWriter, page *Page) {
  err := templates.ExecuteTemplate(*w, name, page)
  if err != nil {
    http.Error(*w, err.Error(), http.StatusInternalServerError)
    return
  }
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    pth := r.URL.Path
    if pth == "/" {
      //we are in root 
      fn (w, r, "")
      return
    }

    //else, handle query
    m := validPath.FindStringSubmatch(pth)
    if m == nil {
      //http.NotFound(w, r)
      http.Redirect(w, r, "/", http.StatusFound)
      return
    }
    t := m[len(m)-1]
    fmt.Printf("Looking for: %s.\n", t)
    fn (w, r, t)
  }
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
  page, err := loadPage(title)
  if err != nil {
    //http.Error(w, err.Error(), http.StatusInternalServerError)
    http.Redirect(w, r, "/edit/"+title, http.StatusFound)
    return
  }

  renderTemplate("view.html", &w, page)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
  page, err := loadPage(title)
  if err != nil {
    //create new page
    page = &Page{Title: title}
  }

  renderTemplate("edit.html", &w, page)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
  body := r.FormValue("form_body")
  fmt.Println("Saving form_body content:")
  fmt.Println(body)
  p := &Page{Title: title, Body: []byte(body)}
  err := p.save()
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  http.Redirect(w, r, "/view/"+p.Title, http.StatusFound)
}

func rootHandler(w http.ResponseWriter, r *http.Request, title string) {
  files, err := ioutil.ReadDir("./pages")
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  var list string
  list += "<div><ul>"
  for _,f := range files {
    fnam := f.Name()
    nam := fnam[:(len(fnam) - len(".txt"))]
    list = list + "<li>" + "<a href='/view/" + nam + "'>" + nam + "</a></li>"
    //fmt.Println(nam)
  }
  list += "</ul></div>"
  fmt.Fprintf(w, list)  //todo make a dynamic list template
}

func main() {
  fmt.Println("Starting server.")
  http.HandleFunc("/", makeHandler(rootHandler))
  http.HandleFunc("/view/", makeHandler(viewHandler))
  http.HandleFunc("/edit/", makeHandler(editHandler))
  http.HandleFunc("/save/", makeHandler(saveHandler))

  http.ListenAndServe(":8080", nil)
}







