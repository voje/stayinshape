package main

import (
	"fmt"
	"github.com/docker/distribution/reference"
	"strings"
)

type ImgRef struct {
	Named reference.Named
}

func GenImgRef(imgStr string) (*ImgRef, error) {
	tn, err := reference.ParseNormalizedNamed(imgStr)
	if err != nil {
		return nil, err
	}
	ret := ImgRef{
		Named: tn,
	}
	return &ret, nil
}

func (tn *ImgRef) Tag() string {
	return strings.Replace(tn.Named.String(), tn.Named.Name(), "", -1)
}

func (tn *ImgRef) Name() string {
	return tn.Named.Name()
}

func (tn *ImgRef) Path() string {
	return reference.Path(tn.Named)
}

func (tn *ImgRef) Domain() string {
	return reference.Domain(tn.Named)
}

func (tn *ImgRef) String() string {
	return tn.Named.String()
}

func main() {
	fmt.Println("Testing docker image reference.")

	imageStrings := []string{
		"gitlab.iskratel.si:4567/voje/gitlab_uploader/gloader:0.0.8",
		"gitlab.iskratel.si:4567/voje/gitlab_uploader/notag",
		"gitlab.iskratel.si:4567/voje/short:1.2.3",
		"from/dockerhub",
	}

	for _, imgStr := range imageStrings {
		ref, err := GenImgRef(imgStr)
		if err != nil {
			panic(err)
		}
		fmt.Printf("---\nFullRef: %s\nAddr: %s\nPath: %s\nTag: %s\n---", ref.String(), ref.Domain(), ref.Path(), ref.Tag())
	}
}
