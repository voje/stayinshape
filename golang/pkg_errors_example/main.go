package main

import (
	"fmt"
	"github.com/pkg/errors"
)

func level1() error {
	fmt.Println("Running level1().")
	err := level2()
	if err != nil {
		return err
	}
	return nil
}

func level2() error {
	fmt.Println("Running level2().")
	fmt.Println("Returning err1.")

	// Using github.com/pkg/erros we can create a New error with stack trace.
	return errors.New("level2 error")

	// In case this is an API call with a pre-defined error, we can add stack trace
	// by calling:
	// return errors.Wrap(someOutsideError, "some description")
}

func main() {
	fmt.Println("Running main.")
	err := level1()
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
}
