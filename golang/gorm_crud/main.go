package main

import (
	"math/rand"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	log "github.com/sirupsen/logrus"
)

type Account struct {
	gorm.Model
	Name string
	Desc string
	Rows []Row
}

type Row struct {
	gorm.Model
	AccountId uint // THIS WAS MISSING!!! // Foreign key.
	Date      string
	Amount    float64
}

func fill(db *gorm.DB) {
	var rows []Row
	for i := 0; i < 1000000; i++ {
		row := Row{
			Date:   "1.4.2044",
			Amount: rand.Float64(),
		}
		rows = append(rows, row)
	}
	acc := Account{
		Name: "Main account",
		Desc: "Main b account.",
		Rows: rows,
	}
	db.Create(&acc)
}

func listAll(db *gorm.DB) {
	var accounts []Account

	// Without preloading Rows, that field will be an empty array.
	db.Preload("Rows").Find(&accounts)
	for _, acc := range accounts {
		log.Printf("%s [%d rows]", acc.Name, len(acc.Rows))
	}
}

func main() {
	// :memory: - temporary in-memory database, use filename if you want to save to file
	// db, err := gorm.Open("sqlite3", ":memory:")
	db, err := gorm.Open("sqlite3", "/tmp/gorm_crud.db")

	// Enable logging -- shows SQL queries.
	db.LogMode(false)

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Make sure we have new tables.
	db.DropTable(&Account{}, &Row{})
	db.CreateTable(&Account{}, &Row{})

	fill(db)

	listAll(db)

	log.Info("GORM test.")

}
