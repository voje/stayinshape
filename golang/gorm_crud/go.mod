module github.com/voje/stayinshape/golang/gorm_crud

go 1.13

require (
	github.com/jinzhu/gorm v1.9.12
	github.com/sirupsen/logrus v1.4.2
)
