```bash
## List Docker CLI commands
docker
docker container --help

## Display Docker version and info
docker --version
docker version
docker info

## Execute Docker image
docker run hello-world

## List Docker images
docker image ls

## List Docker containers (running, all, all in quiet mode)
docker container ls
docker container ls --all
docker container ls -aq
```

## Dwarf fortress
Create a docker image that runs Dwarf Fortress.
```bash
# We need a base image (ubuntu).
$ docker search ubuntu
$ docker pull ubuntu
$ docker run -ti ubuntu /bin/bash
```

Neat way to stop all docker containers: .
Neat way to stop all docker containers: .
Neat way to stop all docker containers: .
Neat way to stop all docker containers: [$ docker rm ].
