# Structure
Two apps and a database.

app1 sending packets on port 6767.
app2 receiving packets and saving them to DB

Done: app1 - sender app; app2 - receiver app.
Create dockerfiles.
Test single docker instances. 
Try compose.

## Progress
Managed to run app1 with a dockerfile.
Use EXPOSE for listening ports only, in our case app1 only emits UDP packets so no EXPOSE. 
Needed to use host network when running the container.  

```bash
$ cd ./app1
# Need a Dockerfile here.
$ docker build . -t exercise1:app1
$ docker run --net=host exercise1:app1

# On host, check if you receive UDP packets.
# UDP port doesn't show up using nmap.
$ netcat -lup 6767
```

## Default bridge, 2 apps on host network. 
Ok, we can see all prints from out python app if we run it like this:
`$ docker run --net=host exercise1:app2`
On the host machine, we can see the same prints if we use: 
`$ docker logs <app2 container name>`
Important part is -it (at least -i, I suppose). `docker logs` prints out whatever is printing in the interactive terminal.  

## User defined bridge. (working example)
All containers on a user defined bridge have ports exposed to each other.  
They should be communicating out of the box. 

App1 is sending UDP packets to `/dev/udp/app2_container/6767`. 
App2 is listening on `(0.0.0.0:6767)` and printing received bytes to terminal. 

```bash
# Create a user-defined bridged network.
$ docker network create ex1_net
# Run the emmitting app. 
# ! Container --name is important for DNS host resolution. 
$ docker run -d --name app1_container --net=ex1_net ex1:app1
# Run receiving app.
$ docker run -d --name app2_container --net=ex1_net ex1:app2
# Also, check out docker logs for same output. 
$ docker logs -f app2_container
```
