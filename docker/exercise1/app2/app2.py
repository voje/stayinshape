#!/usr/bin/python3
# listen on a port for UDP packets. Print them.

import socket

# This only works if we're running the apps on --het=host
# HOST = "127.0.0.1"
# Using a user defined bridge, we have a DNS mapping of container names.
HOST = "0.0.0.0"
PORT = 6767

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind((HOST, PORT))

while True:
    data, addr = sock.recvfrom(1024)
    print(data)

