#!/bin/bash

# Send UDP packets to other container. 
HOST="app2_container"
PORT=6767

while true; do
    echo -n "$(date)" > /dev/udp/"${HOST}"/"${PORT}"
    sleep 3
done

