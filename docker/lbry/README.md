Got it working with the following commands:

* build the image: `docker build -t lbry:init .`
* run with xserver setup: `docker run -it --net host -v $HOME/.Xauthority:/root/.Xauthority --env DISPLAY lbry:init`

It starts a GUI and starts uploading blocks. (takes very long)
