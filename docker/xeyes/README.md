# GUI apps in docker

### Prelude
Let's try to open xeyes in docker, following
[this tutorial](https://medium.com/@SaravSun/running-gui-applications-inside-docker-containers-83d65c0db110)
from Medium.  
After that, try running a python game bot in a docker container while interacting with a host's X window.  

### Basic steps

* Prepare an Ubuntu image.
* Install some programs (python3, xdotool).
* Parameters needed for XServer sharing:
    * `--net=host` (share network - needed for using credentials from .Xauthority),
    * `--volume="$HOME/.Xauthority:/root/.Xauthority:rw"` (see `man xauth`),
    * `--env="DISPLAY"` (share display).

