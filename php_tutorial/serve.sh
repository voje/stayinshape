#!/bin/bash

echo "Watching for changes in index.php."
main_file="index.php"
if (( $# == 1 )); then
  main_file=$1
fi

php -S localhost:8000 &
sid=$!
timestamp=$(stat -c %y $main_file)

while true; do
  ntst=$(stat -c %y $main_file)
  if [ "$timestamp" != "$ntst" ]; then
    echo "Killing $sid."
    kill $sid
    timestamp=$ntst
    echo "Restarting server."
    php -S localhost:8000 &
    sid=$!
  fi
  sleep 1s
done


