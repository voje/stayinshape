<?php
session_start();
if (isset($_SESSION['login'])) {
    header("Location: logged_in.php");
    echo "Logged in as " . $_SESSION['username'];
    exit();
} else {
    header("Location: login.php");
    echo "Please, log in first.";
    exit();
}
?>

<html></html>
