<?php
define("NAME", "tester");
define("PASS", "tpass");
if ( $_SERVER["REQUEST_METHOD"] != "POST" ) {
  echo "Error: wrong request method.";
  exit();
}

if ( $_REQUEST["uname"] == NAME && $_REQUEST["upass"] == PASS ) {
  echo "Login successful.";
  //create cookie
  setcookie("uname", $_REQUEST["uname"], time()+3600, "/", "", 0);
  setcookie("upass", $_REQUEST["upass"], time()+3600, "/", "", 0);
} else {
  echo "Wrong login credentials. Redirecting to login page.";
  sleep(1);
  header("Location: ".$_SERVER["HTTP_REFERER"]);
  exit();
}

?>
