<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cookies</title>
  </head>

  <body>
    <h2>User Login</h2>
    <p>Correct username: tester, pass: tpass.</p>
    <form action="login.php" method="POST">
<?php
$uname_val="";
$upass_val="";
if (isset($_COOKIE["uname"])) {
  $uname_val=$_COOKIE["uname"];
}
if (isset($_COOKIE["upass"])) {
  $upass_val=$_COOKIE["upass"];
}
echo <<< EOT
  <input type="text" name="uname" placeholder="name" value="$uname_val"/>
  <input type="text" name="upass" placeholder="password" value="$upass_val"/>
  <input type="submit"/>
EOT;
?>
    </form>
  </body>
</html>
