<?php
session_start();
echo $_SERVER["PHP_SELF"];
if (!(isset($_SESSION))) {
  $_SESSION["counter"] = 1;  
} else {
  $_SESSION["counter"] += 1;
}
//session_destroy(); resets counter to 1 each time
?>

<html>
<head>
<title>Sessions</title>
<meta charset="utf-8">
</head>

<body>
<p>
<?php
echo("Session conter: " . $_SESSION["counter"] . "<br/>");
echo("SID: " . SID);
?>
</p>
<p>Session via URL: 
<a href="nextPage.php?<?php echo htmlspecialchars(SID) ?>">nextPage.php</a>
</p>
</body>
</html>
