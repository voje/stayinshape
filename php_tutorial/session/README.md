## SID
When cookies are disabled, the client communicates its PHPSESSION information
via URL: url?session_id=1234123412.  

This behavior should be automatic. The const SID is changed from "" to
"sessoin_id=123412342" and can be used attached to a redirection on our page.  
Alas, I wasn't able to establish the forementioned behavior on my test example. To await further tinkering.  
