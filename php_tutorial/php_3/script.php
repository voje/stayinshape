<?php
  define("TXT", "This is a test text.\n");
  define("FILENAME", "test.txt");
  echo "<p>" . FILENAME . "<br/>";
  echo TXT . "<br/>";
  $exsts = file_exists(FILENAME);
  echo "File exists: "; echo $exsts ? "true":"false"; echo "</p><br/>";
  $file = fopen(FILENAME, "w"); //addd "w", to create file
  if ( $file == false ) {
    echo "<p>Error opening file " . FILENAME . ".</p>";
    exit();
  }
  fwrite($file, TXT);
  fclose($file);

  //read
  $file = fopen(FILENAME, "r");
  if ( $file == false) {
    echo "Error opening file " . FILENAME . ".";
    exit();
  }
  $filesize = filesize( FILENAME ); //weird...
  $text = fread( $file, $filesize );
  fclose($file);
  echo "<p>File contains: </p>";
  echo "<pre>$text</pre>";
?>
