# PHP (PHP: Hypertext Preprocessor)
Overview from tutorialspoint.com

## Run php locally
I won't be using a server, I'll rather use the command ```$ php -S localhost:8000 index.php``` to run the scripts.  
Database will be postgres. User: phptut, passwd: phptut.  

## Script to restart php server
Runs ```$php -S localhost:8000``` when file changes.  
Run it from a folder containing ```index.php```.  
```$../script.sh```

TODO: on script restart, we get error: Failed to listen on localhost:8000
(reason: Address already in use)

