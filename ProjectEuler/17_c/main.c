#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char* ones[] = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    char* tens[] = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
    char* otherTens[] = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
    char* thou = "onethousand";
    char* empty = "";

    int n_chars = 0;

    //hundreds
    int i; 
    for ( i=0; i<10; i++ ) {
        char* tmpOnes = ones[i];
        char* hun = "hundred";
        if ( i == 0 ) {
            hun = "";
        } else {
            hun = "hundred";
        }

        int j;
        for ( j=0; j<10; j++ ) {
            char* tmpOT =  otherTens[j];

            int k;
            for ( k=0; k<10; k++ ) {
                char* tmpOnes1 = ones[k];
                char* and = "and";
                if ( (i == 0) || (j == 0 && k == 0) ) {
                    and = "";
                } else {
                    and = "and";
                }

                char* num;
                size_t num_size = strlen(tmpOnes) + strlen(hun) + strlen(and) + strlen(tmpOT) + strlen(tmpOnes1);
                num = malloc( num_size + 1 );
                num = strcat( num, tmpOnes );
                num = strcat( num, hun );
                num = strcat( num, and );
                if ( j == 1 ) {
                    num = strcat( num, tens[k] );
                } else {
                    num = strcat( num, tmpOT );
                    num = strcat( num, tmpOnes1 );
                }

                n_chars += strlen( num );
                /*
                printf("%s\t\t", num);
                if ( k%5 == 0 ) {
                    printf("\n");
                }
                */
            }
        } 

    } //counts from 1 to 999

    n_chars += strlen( thou );

    printf( "Number of characters if we count from 1 to 1000: \n%d\n", n_chars );

    return 0;
}