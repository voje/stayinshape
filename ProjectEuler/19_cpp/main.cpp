#include <iostream>
#include <cstdio>
#include <stdlib.h>

using namespace std;

class Date {
private:
	int year;
	int month;
	int day;	
	int dayOfTheWeek;
public:
	Date();
	void increment();
	void check(int&);
	string toString();
};

Date::Date(void) {
	this->year = 1900;
	this->month = 1;
	this->day = 1;
	this->dayOfTheWeek = 0; //0 is monday
}

string Date::toString() {
	char* str = (char*)malloc(7*sizeof(char));
	sprintf(str, "%2d.%2d.%4d", this->day, this->month, this->year);
	return string(str);
}

void Date::check(int& dayCounter) {
	if (this->year >= 1901 && this->day == 1 && this->dayOfTheWeek == 6) {
		++dayCounter;
	}
}

void Date::increment() {
	++(this->day);
	this->dayOfTheWeek = (this->dayOfTheWeek + 1) % 7;
	int monthDays;
	if (this->month == 2) {
		if (this->year % 100 == 0) {
			if (this->year % 400 == 0) {
				monthDays = 28;
			} else {
				monthDays = 29;
			}
		} else {
			if (this->year % 4 == 0) {
				monthDays = 29;
			} else {
				monthDays = 28;
			}
		}
	} else if (month == 4 || month == 6 || month == 9 || month == 11 ) {
		monthDays = 30;
	} else {
		monthDays = 31;
	}

	if (this->day > monthDays) {
		this->day = 1;
		++(this->month);
	}
	if (this->month > 12) {
		this->month = 1;
		++(this->year);
	}
}


int main() {
	int dayCounter = 0;
	Date dt = Date();
	cout << dt.toString() << endl;

	dt.check(dayCounter);
	while (dt.toString() != "31.12.2000") {
		dt.increment();
		dt.check(dayCounter);
		cout << dt.toString() << endl;
	}
	dt.check(dayCounter);

	cout << dayCounter << endl;

	return 0;
}