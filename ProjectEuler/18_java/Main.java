import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        //read the damn file 
        String filename = "input1.txt";
        if (args.length > 0) {
            filename = args[0];
        }
        Scanner sc = new Scanner(new File(filename));
        Node root = null;
        ArrayList<Node> freeParents = new ArrayList<Node>();
        int height = 0;
        int maxVal = 0;
        while (sc.hasNextLine()) {
            String ln = sc.nextLine(); 
            Scanner linesc = new Scanner(ln);
            boolean firstInt = true;
            while (linesc.hasNextInt()) {
                int val = linesc.nextInt(); 
                if (val > maxVal) {
                    maxVal = val;
                }
                boolean lastInt = !linesc.hasNextInt();
                //in case of empty tree
                Node newNode = new Node(val, height);
                if (freeParents.size() == 0) {
                    root = newNode;
                } else {
                    if (!firstInt) {
                        if (!lastInt) {
                            //middle of binome
                            Node lParent = freeParents.get(0);
                            Node rParent = freeParents.get(1);
                            lParent.rChild = newNode;
                            freeParents.remove(0);
                            rParent.lChild = newNode;
                        } else {
                            //right edge of binome
                            Node lParent = freeParents.get(0);
                            lParent.rChild = newNode;
                            freeParents.remove(0);
                        }
                    } else {
                        //left edge of binome
                        Node rParent = freeParents.get(0);
                        rParent.lChild = newNode;
                    }
                }
                freeParents.add(newNode);
                firstInt = false;
            }
            height ++;
        }

        //set inverse values
        setInverseValues(root, maxVal);

        printTree(root);
        AStar as = new AStar(root);
        as.calcPath();
    }

    public static void printTree(Node root) {
        ArrayList<Node> leaves = new ArrayList<Node>();
        leaves.add(root);
        boolean stop = false;
        while(!stop) {
            int s = leaves.size();
            for (int i=0; i<s; i++) {
                Node tmp = leaves.remove(0);
                System.out.print(tmp.toString());
                switch (tmp.nChildren()) {
                    case 0:
                        stop = true;
                        break;
                    case 1:
                        if (!leaves.contains(tmp.lChild))
                            leaves.add(tmp.lChild);
                        break;
                    case 2:
                        if (!leaves.contains(tmp.lChild))
                            leaves.add(tmp.lChild);
                        if (!leaves.contains(tmp.rChild))
                            leaves.add(tmp.rChild);
                        break;
                }
            }
            System.out.println();
        }  
    }

    public static void setInverseValues(Node root, int maxVal) {
        ArrayList<Node> leaves = new ArrayList<Node>();
        leaves.add(root);
        boolean stop = false;
        while(!stop) {
            int s = leaves.size();
            for (int i=0; i<s; i++) {
                Node tmp = leaves.remove(0);
                tmp.inverseValue = maxVal - tmp.value;
                switch (tmp.nChildren()) {
                    case 0:
                        stop = true;
                        break;
                    case 1:
                        if (!leaves.contains(tmp.lChild))
                            leaves.add(tmp.lChild);
                        break;
                    case 2:
                        if (!leaves.contains(tmp.lChild))
                            leaves.add(tmp.lChild);
                        if (!leaves.contains(tmp.rChild))
                            leaves.add(tmp.rChild);
                        break;
                }
            }
            System.out.println();
        }  
    }
}   //Main

class Node {
    int height, value, inverseValue;
    Node lParent, rParent;
    Node lChild, rChild;

    public Node(Node lParent, Node rParent, int value) {
        this.value = value;
        this.inverseValue = -1;
        this.lParent = lParent;
        this.rParent = rParent;

        //height
        if (this.lParent == null ) {
            if (this.rParent == null) {
                this.height = 0; //root node
            } else {
                this.height = this.rParent.height + 1;
            }
        } else {
            this.height = this.lParent.height + 1;
        }
    }

    public Node(int value, int height) {
        this.height = height;
        this.value = value;
        this.inverseValue = -1;
        this.lParent = null;
        this.rParent = null;
    }



    public int nChildren() {
        int n = 0;
        if (this.lChild != null)
            n++;
        if (this.rChild != null)
            n++;
        return n;
    }

    public String toString() {
        String ret = "( "+this.value+", "+this.height+" )  ";
        return ret;
    }
}

class Path {
    int g;
    ArrayList<Node> nodes;

    //root init
    public Path(Node node) {
        this.nodes = new ArrayList<Node>();
        this.nodes.add(node);
        this.g = node.inverseValue;
    }
    public Path(Node newNode, ArrayList<Node> nodes, int g) {
        this.nodes = new ArrayList<Node>(nodes);
        this.nodes.add(newNode);
        this.g = g;
        this.g += newNode.inverseValue;
    }
    public Node getLastNode() {
        if (this.nodes.size() == 0) {
            return null;
        }
        return this.nodes.get(this.nodes.size()-1);
    }
    public int getPathValue() {
        int ret = 0;
        for (int i=0; i<this.nodes.size(); i++) {
            ret += this.nodes.get(i).value;
        }
        return ret;
    }
    public String toString() {
        String ret = "Path: ";
        for (int i=0; i<this.nodes.size(); i++) {
            ret += this.nodes.get(i).toString();
        }
        return ret;
    }
}

class AStar {
    int maxHeight, maxPath;
    ArrayList<Node> pathNodes;
    Node root;
    public AStar(Node root) {
        this.root = root;
        this.maxHeight = this.getMaxHeight();
        this.maxPath = this.maxHeight;
        this.pathNodes = new ArrayList<Node>();
    } 

    private int getMaxHeight() {
        int maxH = 0;
        Node tmp = this.root;
        while (!(tmp.lChild == null)) {
            maxH ++;
            tmp = tmp.lChild;
        }
        return maxH;
    }

    private int bestPathIndex(ArrayList<Path> al) {
        int bestIndex = 0;
        int minVal = 100000;
        for (int i=0; i<al.size(); i++) {
            Path tmp = al.get(i);
            int tmpVal = tmp.g + (this.maxHeight - tmp.nodes.get(tmp.nodes.size()-1).height);
            if (tmpVal < minVal) {
                minVal = tmpVal;
                bestIndex = i;
            }
        }
        return bestIndex;
    }

    public void calcPath() {
        ArrayList<Path> paths = new ArrayList<Path>();
        boolean reachedLeaves = false;
        int tries = 0;
        while (!reachedLeaves) {
            tries ++;
            if (paths.size() == 0) {
                paths.add(new Path(root));
            } else {
                int idx = bestPathIndex(paths);
                Path tmpPath = paths.get(idx);
                Node tmpNode = tmpPath.getLastNode();
                if (tmpNode.nChildren() == 0) {
                    reachedLeaves = true;
                } else {
                    paths.add(new Path(tmpNode.lChild, tmpPath.nodes, tmpPath.g));  
                    paths.add(new Path(tmpNode.rChild, tmpPath.nodes, tmpPath.g));  
                    paths.remove(0);
                }
            }
        } 
        System.out.printf("Tries: %d\n", tries);
        int best = bestPathIndex(paths);
        System.out.printf("Best path: %d\n", paths.get(best).getPathValue());
        System.out.println(paths.get(best).toString());
    }
}

