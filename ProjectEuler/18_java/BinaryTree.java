import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class Main {
    public static void printTree(Node root) {
        ArrayList<Node> waiting = new ArrayList<Node>();
        waiting.add(root); 

        while(!waiting.isEmpty()){
            int nParents = waiting.size();
            for (int i=0; i<nParents; i++) {
                Node tmp = waiting.remove(0);
                System.out.print(tmp.toString());
                if (tmp.lChild != null)
                    waiting.add(tmp.lChild);
                if (tmp.rChild != null)
                    waiting.add(tmp.rChild);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) throws Exception {
        //read the damn file 
        String filename = "input1.txt";
        Scanner sc = new Scanner(new File(filename));
        Node root = null;
        ArrayList<Node> freeParents = new ArrayList<Node>();
        while (sc.hasNextLine()) {
            String ln = sc.nextLine(); 
            Scanner linesc = new Scanner(ln);
            boolean firstInt = true;
            while (linesc.hasNextInt()) {
                int val = linesc.nextInt(); 
                boolean lastInt = !linesc.hasNextInt();
                if (freeParents.size() == 0) {
                    root = new Node(null, val);
                    freeParents.add(root);
                } else {
                    Node tmp = freeParents.get(0);
                    freeParents.add(new Node(tmp, val));
                    if (tmp.nChildren() == 2) {
                        freeParents.remove(0);
                    }
                }
                firstInt = false;
            }
        }

        printTree(root);

    }
}   //Main

class Node {
    int height, value;
    Node parent, lChild, rChild;

    public Node(Node parent, int value) {
        this.value = value;
        if (parent == null)
            this.height = 0;
        else
            this.height = parent.height + 1;
        this.lChild = null;
        this.rChild = null;
        this.parent = parent;
        if (this.parent != null) {
            if (this.parent.lChild == null)
                this.parent.lChild = this;
            else if (this.parent.rChild == null)
                this.parent.rChild = this;
            else
                System.out.println("ERROR: parent already has two children.");
        }

    }


    public int nChildren() {
        int n = 0;
        if (this.lChild != null)
            n++;
        if (this.rChild != null)
            n++;
        return n;
    }

    public String toString() {
        String ret = "( "+this.value+", "+this.height+" )  ";
        return ret;
    }
}

class AStar {

}

