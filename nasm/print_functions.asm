; Library contains printing functions: fun_printstr, fun_println, fun_printchar, fun_printint
; You can't compile this file directly. (You can create a .o file, I suppose)
; To include in a program, use this command:
; %include  'print_functions.asm'

fun_printstr:
    ;arguments: ecx contains address of string, edx contains length of string
    push    eax
    push    ebx
    push    ecx
    push    edx

    mov     eax, 4
    mov     ebx, 1
    int     0x80
    
    pop     edx
    pop     ecx
    pop     ebx
    pop     eax
    ret
;end of fun_printstr
    
fun_println:
    push    eax
    push    ebx
    push    ecx
    push    edx
    
    mov     eax, 0x0A ;newline char
    push    eax     ;spodaj v fun_printchar sem zakomentiral kaj tole vse dela
    
    mov     eax, 4
    mov     ebx, 1
    mov     ecx, esp    ;pointer na stack, kjer zdaj sedi vrednost eax
    mov     edx, 1
    int     0x80
    
    pop     eax
    
    pop     edx
    pop     ecx
    pop     ebx
    pop     eax
    ret
;end of fun_println

fun_printint:
    push    eax
    push    ebx
    push    ecx
    push    edx
    
    mov     ecx, 0 ; byte counter
    mov     ebx, 10 ; divider
    
divideloop:
    mov     edx, 0 ; clear
    div     ebx ; eax / ebx = eax + edx
    push    edx
    add     ecx, 1
    cmp     eax, 0
    jnz     divideloop ; if eax isn't zero after division, repeat
    
printloop:
    pop     eax
    add     eax, 48
    call    fun_printchar
    sub     ecx, 1
    cmp     ecx, 0
    jnz     printloop
    
    pop     edx
    pop     ecx
    pop     ebx
    pop     eax
    ret
;end of fun_printint

fun_printchar:
    ;arguments: eax vsebuje char, ki ga tiskamo
    push    eax ;teli pushi shranijo vrednosti registrov, da jih bomo lahko obnovili, ko se vrnemo iz funkcije
    push    ebx
    push    ecx
    push    edx
    
    ;ta fora: sistemska funkcija hoce, da ji podamo naslov, kjer se nahaja char, tako da ga ne moremo
    ;preprosto dati v register. Lahko bi ga shranili v dodeljen prostor zgoraj v .data...
    ;lahko pa ga porinemo na stack in si shranimo naslov na stacku (pointer na stack je esp)
    ;namesto pointerja na naslov v .data podamo pointer na naslov na stacku
    push    eax
    
    ;--- sistemski klic (eax, ebx, ecx, edx so argumenti, ind 0x80 je klic sistemu)
    mov     eax, 4      ;4 je sys_write
    mov     ebx, 1      ;1 je izhod (1 == stdout)
    mov     ecx, esp    ;pointer na stack, kjer zdaj sedi vrednost eax
    mov     edx, 1      ;edx vsebuje length stringa, tokrat 1
    int     0x80
    ;---
    
    pop     eax         ;pobrisemo vrednost dol s stacka
    
    pop     edx         ;nazaj obnovimo ostale registre.
    pop     ecx         ;obraten vrstni red, kot smo jih pushali
    pop     ebx
    pop     eax
    ret
;end of fun_printchar
    
    
