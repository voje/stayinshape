section .text

;name _start is obligatory (tells kernel where to start)
global  _start

_start: 
    call    greeting

    mov ebx, 0
    mov eax, 1
    int 0x80

greeting:
    push    eax
    push    ebx
    push    ecx
    push    edx
    
    mov     eax, 4
    mov     ebx, 1
    mov     ecx, msg
    mov     edx, len
    int     0x80

    pop     edx
    pop     ecx
    pop     ebx
    pop     eax 
    ret

section .data
msg db  'Greetings from a subroutine!', 0x0A
len equ $-msg
