%include    'print_functions.asm'

section     .text

global  _start


;main()... sort of...
_start:
    ; prepare 3 values to compare
    mov     eax, 123
    mov     ebx, 555
    mov     ecx, 234

    ; result will be in eax
    call    fun_compare3

    ; print contents of eax
    call    fun_printint
    call    fun_println
    
    ;exit program
    mov     ebx, 0
    mov     eax, 1
    int     0x80
;end of program


fun_compare3:
    ;arguments in eax, ebx, ecx, result in eax

    ;cmp stores flags in special registers
    cmp     eax, ebx

    ;jg will read those flags
    ;(jump to c1 if eax greater then ebx)
    jg      c1

    ;else eax = ebx
    mov     eax, ebx

c1: 
    ;same with eax and ecx, biggest number stays in eax
    cmp     eax, ecx
    jg      c2
    mov     eax, ecx

c2: 
    ret
;end of fun_compare3

