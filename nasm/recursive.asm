%include 'print_functions.asm'

section .text

global _start

;_start je zgleda tako kot main()
_start:
    mov     eax, 27
    mov     ebx, 23
    call    fun_recursive

    mov     eax, 1
    mov     ebx, 0
    int     0x80
;end of _start / main() / whatever :P

fun_recursive:
    ;arguments: eax current number, ebx terminator number
    
    push    eax     ;the only register the function is changing
    
    call fun_printint   ;print current eax
    call fun_println
    
    cmp     eax, ebx
    jz      exit_recursive  ;if eax == ebx, don't call recursion anymore, jump to exit
    
    ;else...
    sub     eax, 1
    call    fun_recursive
    
exit_recursive:
    pop     eax
    ret
;end of fun_recursive
