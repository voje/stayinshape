%include    'print_functions.asm'   ;in separate file for usage across programs

section .data
msg     db  "Test hello world!"
len     equ $-msg   ;fora, da length of msg izracunamo tako, da odstejemo naslova v pomnilniku. ($ je naslov te vrstice)

section .text

global _start

;_start je zleda tako kot main()
_start:
    ;test printint, ki delji stevilo z 10, da dobi ven decimalki in jih natisne kot char (vrednost int + 48)
    mov     eax, 1342   ;pripravi argument
    call    fun_printint    ;klici funkcijo
    call    fun_println
    
    ;test print string, new line, string
    mov     ecx, msg    ;argument1
    mov     edx, len    ;argument2
    call    fun_printstr    ;klic
    call    fun_println
    call    fun_printstr
    call    fun_println
    
    ;test natisni en char
    mov     eax, 't'
    call    fun_printchar
    call    fun_printchar
    call    fun_printchar
    call    fun_println
    call    fun_printchar
    call    fun_printchar
    call    fun_println
    call    fun_printchar
    call    fun_println
    
    mov     eax, 1
    mov     ebx, 0
    int     0x80
;end of _start / main() / whatever :P

