# Playing around with nasm
I used the lines in `buildme.sh` to compile and run programs on Manjaro.  

If you use the script, input need an extension (`.asm`) preferably.  

```bash
$ ./buildme print_functions.asm
```
