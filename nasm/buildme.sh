#!/bin/bash

# Strin extension from name
name=$(echo $1 | cut -d'.' -f1)

nasm -f elf $1
ld -m elf_i386 "${name}.o" -o "${name}"
