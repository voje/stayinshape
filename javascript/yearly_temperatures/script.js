//read data
var data = {};
var x_ticks = [];
var min_tmp = -5;
var max_tmp = 30;

loadData("./lj_bezigrad_meritve.csv", drawGraph);

function checkData() {
    console.log(data);
}

function loadData(filename, callback) {
    d3.csv(filename, function(indata) {
    	//create ticks
    	for (item in indata[0]) {
    		if (item != "LETO") {
    			x_ticks.push(item);
    		}
    	}
    	for (leto in indata) {
    		var yy = indata[leto].LETO;
            data[yy] = {};
            data[yy].year = yy;
    		data[yy].temperatures = [];
    		delete indata[leto].LETO;
    		var tmp = indata[leto];
    		for (month in tmp) {
                /*
                WTF DELA TUKAJ?? TO JE PREPROST "najdi minimum" !!!!
                console.log(min_tmp);
                if (tmp[month] < min_tmp) { console.log("new min: " + tmp[month]); min_tmp = tmp[month] };
                if (tmp[month] > max_tmp) { max_tmp = tmp[month] };
                */
    			data[yy].temperatures.push({ x: month, y: tmp[month] });
    		}
    	}
	    callback();	//blooody callbacks...
    });
}

/*
data = {
    [
    year: 2001,
    temperatures: [ {0, tmpjan}, {1, tmpfeb}, {2, tmpmar}, ... ],
    ],
	...
}
*/

function drawGraph() {

    var margin = { top: 30, right: 20, left: 50, bottom: 70 };
    var height = 270 - margin.top - margin.bottom;
    var width = 600 - margin.left - margin.right;

    var xScale = d3.scale.ordinal()
    	.rangeRoundBands([0, width], 1)
    	.domain(x_ticks);
    var yScale = d3.scale.linear()
    	.range([height, 0])
    	.domain([min_tmp, max_tmp]);

    var xAxis = d3.svg.axis().scale(xScale).orient("bottom");
    var yAxis = d3.svg.axis().scale(yScale).orient("left");

	var lineFunction = d3.svg.line()
                         .x(function(d) { return xScale(d.x); }) //todo
                         .y(function(d) { return yScale(d.y); })
                         .interpolate("linear");

    var colors = d3.scale.category20();

    //canvas
    var svgContainer = d3.select("body")
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", "translate("+margin.left+", "+margin.top+")");

    var tooltip = d3.select("body")
        .append("div")
        .attr("class", "tooltip");

    //axis
    svgContainer.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate("+0+", "+height+")")
        .call(xAxis)
        //orient ticks
        .selectAll("text")	
                    .style("text-anchor", "end")
                    .attr("dx", "-.8em")
                    .attr("dy", ".15em")
                    .attr("transform", function(d) { return "rotate(-65)" });

    svgContainer.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    //lines
    for (year in data) {
        svgContainer.append("path")
            .datum(data[year])  //data for points, datum for whole data(path, ..)
            .attr("d", lineFunction(data[year].temperatures))
            .attr("stroke", function() { return colors.range()[year%20]; })
            .attr("stroke-width", 1)
            .attr("fill", "none")
            .attr("class", "line line_" + year)
            .on("mouseover", function(d) {
                //we could calculate yvalue but not by using path : use mouseposition
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 0.9);
                tooltip.html("<p>"+d.year+"</p>")
                    .style("left", d3.event.pageX + "px")
                    .style("top", (d3.event.pageY - 50) + "px")
                    .style("background-color", function() { return colors.range()[(d.year)%20]; });
            })
            .on("mouseout", function(d) {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    }

}