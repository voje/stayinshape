# React

Going back to react. 
The goal is to build a static photo gallery.  

Website should be hidden, accessable via long obscure link (similar to Tor sites).   
Photos are links to google drive images.  

To get started, install a few things:
```bash
# Check the official site for installation.
$ apt-get install nodejs
$ npm i -g create-react-app

# Create the boilerplate.  
$ create-react-app helloworld

# Run a development server on :3000
$ cd helloworld
$ npm run
```