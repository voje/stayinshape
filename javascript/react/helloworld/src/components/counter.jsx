import React, { Component } from 'react';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: this.props.value,
        }
    };
    render() { 
        return (
            <div>
                <button 
                    className="btn m-2 btn-primary"  
                    onClick={this.handleIncrement}>Count: {this.state.count}</button>
                <button 
                    className={this.getResetBtnClass()}
                    onClick={this.handleResetCounter}>Reset</button>
            </div>
        );
    };

    getResetBtnClass = () => {
        let ret = "btn m-2 btn-danger "
        if (this.state.count === 0) {
            ret += "disabled"
        }
        return ret
    }

    handleIncrement = () => {
        this.setState({
            count: this.state.count + 1,
        })
    };

    handleResetCounter = () => {
        this.setState({
            count: 0,
        })
    }
}
 
export default Counter;