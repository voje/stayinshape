import React, { Component } from 'react';
import Counter from "./counter"

var id = 0;
function newId() {
    id += 1;
    return id - 1;
}

class Counters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counters: [
                { value: 0 },
                { value: 4 }, 
                { value: 9 },
            ]
        }
    }
    render() { 
        return (
            <div>
                { this.state.counters.map(
                    counter => 
                    <Counter key={newId()} value={counter.value} />
                ) }
            </div>
        )
    }
}
 
export default Counters;