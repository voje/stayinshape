import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Counters from "./components/counters";
import Thumbnail from "./components/thumbnail";

const logo = "https://i.ya-webdesign.com/images/biohazard-transparent-hazardous-5.png"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Counters />
        <Thumbnail />
      </header>
    </div>
  );
}

export default App;
