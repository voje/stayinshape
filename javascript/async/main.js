console.log('Testing JS promises.');

console.log('You create a promise object. Argument is an executor function.');
const fetchData = new Promise(t, (resolve, reject) => {
	await sleep(t);
	success = true;
	if (success) {
		resolve();
	} else {
		reject();
	}
});

fetchData.then(() => {
	console.log('Succeeded in fetching data.');
},
() => {
	console.log('Fail.');
});