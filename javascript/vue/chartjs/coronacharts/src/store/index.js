import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

function r256() {
  return Math.floor(Math.random() * 256);
}

function randomColor() {
  return `rgb(${r256()},${r256()},${r256()},0.7)`;
}

export default new Vuex.Store({
  state: {

    // Switching true/false seems to get optimized out of existence.
    // Triggering a renderRequest by incrementing.
    renderRequest: 0,

    selCntryIds: [],

    // List of countries.
    locations: [],
    colors: {},

    latest: {
      confirmed: 0,
      deaths: 0,
      recovered: 0,
    },

    // Timeline for specific country
    timelines: {
      mockId: {
        country: 'Canada',
        countryCode: 'CA',
        lastUpdated: 'date',
        latest: {
          confirmed: 0,
          deaths: 0,
          recovered: 0,
        },
        province: 'province',
        timelines: {
          confirmed: {
            timeline: {
              date: 0,
            },
          },
          deaths: {
            timeline: {
              date: 0,
            },
          },
          recovered: {
            timeline: {
              date: 0,
            },
          },
        },
      },
    }, // timelines
  },
  mutations: {
    saveLocations(state, data) {
      state.latest = data.latest;
      state.locations = data.locations;

      // Generate uniq colors.
      state.locations.forEach((x) => {
        state.colors[x.id] = randomColor();
      });
    },
    saveTimeline(state, data) {
      state.timelines[data.id] = data;
    },
    saveSelCntryIds(state, data) {
      state.selCntryIds = data;
    },

    // All the charts should watch 'renderRequest'
    requestRender(state) {
      state.renderRequest += 1;
    },
  },
  actions: {
    getLocations() {
      Vue.axios.get('https://coronavirus-tracker-api.herokuapp.com/v2/locations')
        .then((success) => {
          this.commit('saveLocations', success.data);
        });
    },
    // This action triggers when a new country is selected.
    // Fetch data from API only if missing.
    updateSelCntryIds(ctx, { selCntryIds }) {
      ctx.commit('saveSelCntryIds', selCntryIds);
      const promises = [];
      ctx.state.selCntryIds.forEach((id) => {
        if (!(id in ctx.state.timelines)) {
          promises.push(Vue.axios.get(`https://coronavirus-tracker-api.herokuapp.com/v2/locations/${id}?timelines=1`));
        }
      });
      Promise.all(promises).then((data) => {
        data.forEach((x) => {
          ctx.commit('saveTimeline', x.data.location);
        });
        ctx.commit('requestRender');
      });
    },
  },
});
