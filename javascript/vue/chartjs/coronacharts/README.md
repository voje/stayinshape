# coronacharts

Using this open corona tracker API:
```
https://github.com/ExpDev07/coronavirus-tracker-api
```
Select country -> display confirmed cases: daily (bar chart) and compound (line area chart).
Compare up to 4 country graphs on the same page.

Using: Vue.js and Vuetify.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
