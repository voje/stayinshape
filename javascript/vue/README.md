# Setps to create a vue.js project

* Initiate a project.
```bash
$ npm install -g vue-cli
$ vue init webpack my-project
$ cd my-project
$ npm install
$ npm run dev
```

* Optionally, add dependencies.
```bash
$ npm install --save some-dependencie
```

* Build for production. 
Compressed files are located in `./dist`. 
TODO: check if they run without the `./node_modules` directory (probably not though). 
```bash
$ npm run build
```
