import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Vanilla from '../views/Vanilla.vue';
import UsingVuex from '../views/UsingVuex.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/vanilla',
    name: 'vanilla',
    component: Vanilla,
  },
  {
    path: '/using-vuex',
    name: 'usingVuex',
    component: UsingVuex,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
