import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    hello: "Root level.",
    nested1: { value: "First level." },
    nested2: {
        n1: {
            name: "nn1",
            value: "Testing a nested property.",
        },
        n2: {
            name: "nn2",
            value: "Testing second nested property.",
        },
    },
    nestedComponents: {
        dummy: {
            name: "dummy",
            text: "dummy",
        },
        /*
        c1: {
            name: "nested component 1",
            text: "testing 123",
        },
        c2: {
            name: "nested component 2",
            text: "testing 123",
        },
        */
    },
  },
  mutations: {
    generateProps(state) {
        // First, modify some values.
        state.hello = "Modified.";
        state.nested1.value = "Modified.";
        Object.keys(state.nested2).forEach((key) => {
            state.nested2[key].value = "Modified";
        });

        // Second, add some props (we're expecting failure here).
        state.nested2["n3"] = {
            name: "Added row.",
            value: "Added row.",
        };

        let generated = "c3";

        // Adding components
        state.nestedComponents[generated] = {
            name: "generated component",
            text: "generated text",
        };

        // Change the GENERATED component's properties.
        state.nestedComponents[generated].text = "Modified";

        // Works like a charm...
    },
    saveNestedComponents(state, data) {
        state.nestedComponents = data;
    },
    modifyDynamicallyGeneratedData(state) {
        state.nestedComponents["c1"].name = "c1**";
    },
  },
  actions: {
    fetchApiData() {
        let nestedComponents = {
            c1: {
                name: 'c1',
                text: 'c1 content',
            },
            c2: {
                name: 'c2',
                text: 'c2 content',
            },
        };
        this.commit('saveNestedComponents', nestedComponents);
    },
  },
  modules: {
  }
})
