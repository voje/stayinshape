# reactivity
Debugging the issue when components created with dynamically generated
data don't watch for changes.

Vue needs to initialize its data with a deterministic structure.
That means we can't just put 'componentData = {}' in our store and fill it in runtime -- in that case vue will display the data correctly but it won't set up watchers which means any changes to the data won't be reflected in view.

The most simple fix is to define a dummy object in data:
```js
componentData = {
    dummy: {
        title: 'title';
        text: 'text';
        someVal: 'someVal';
    },
}
```
When we fetch data from the API, simply rewrite the 'componentData' variable.
