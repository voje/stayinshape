import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)

import routes from './routes'

const router = new VueRouter({
  mode: 'history',  // Remove ugly hash from URL.
  routes: routes,
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
