import HelloWorld from "./components/HelloWorld"
import Home from "./components/Home"

const routes = [
    {
        path: "/",
        redirect: "/home",
    },
    {
        path: "/vue-flair",
        component: HelloWorld,
    },
    {
        path: "/home",
        component: Home,
    },
    {
        path: "*",
        redirect: "/home",
    },
];

export default routes