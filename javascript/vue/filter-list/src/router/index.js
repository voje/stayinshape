import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import FilterList from '@/components/FilterList'
import Bench from '@/components/Bench'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/filter-list',
      name: 'FilterList',
      component: FilterList
    },
    {
      path: '/bench',
      name: 'Bench',
      component: Bench
    }
  ]
})
