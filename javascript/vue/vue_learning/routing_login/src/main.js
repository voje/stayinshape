// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from "bootstrap-vue"

// test thist
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)

const store = {
    user: null
}

const storeMethods = {
    storeSet: function(field, val) {
        store[field] = val
    },
    storeGet: function(field) {
        return store[field]
    },
    loggedIn: function() {
        return store.user != null
    },
    logOut: function() {
        store.user = null
        this.$router.push("/")
    }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  data: store,
  methods: storeMethods
})
