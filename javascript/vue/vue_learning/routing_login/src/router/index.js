import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Landing from "@/components/Landing"
import Guests from "@/components/Guests"
import Login from "@/components/Login"
import Restricted from "@/components/Restricted"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing 
    },
    {
        path: "/guests",
        name: "Guests",
        component: Guests
    },
    {
        path: "/login",
        name: "Login",
        component: Login 
    },
    {
        path: "/restricted",
        name: "Restricted",
        component: Restricted
    }
  ]
})
