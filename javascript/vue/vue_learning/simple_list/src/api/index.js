var data = {
  events: [
    {
      date: '2.6.2018',
      member: 'Kristjan',
      name: 'Pizza party'
    },
    {
      date: '8.6.2018',
      member: 'Jaka K',
      name: 'Gimnazijski koncert'
    },
    {
      date: '9.6.2018',
      member: 'Aljosa',
      name: 'Oglasbena Loka'
    }
  ]
}

export function getData () {
  return data
}
