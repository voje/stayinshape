# Write a simple todo list #

```html
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
```

## Angular directives extend HTML attributes ##

* ng-app
* ng-init
* ng-model
* ng-bind {{ }}
* ng-controller
* <ng-repeat="x in names">
* When naming a directive, you must use a camel case name, w3TestDirective, but when invoking it, you must use - separated name, w3-test-directive
* $rootScope belongs to ng-app, $scope belongs to individual ng-controller




