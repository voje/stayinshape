var WIDTH = 600;
var HEIGHT = 400;

var data = [
	{ name: "E", parents: ["D", "C"], description: "John Lennon" },
	{ name: "D", parents: ["A"], description: "Paul McCartney" },
	{ name: "C", parents: ["A"], description: "Ringo Starr" },
	{ name: "B", parents: ["A"], description: "George Harrison" },
	{ name: "A", parents: [], description: "Jude" }	
];


var nodes = [];

var links = [];

var index_map = {};
function create_index_map() {
	for (n in data) {
		index_map[data[n].name] = n;	
	}
}

function parse_data() {
	create_index_map();
	for (n in data) {
		var node = data[n];
		nodes.push({ name: node.name, description: node.description });
		for (p in node.parents) { 
			var parent = node.parents[p];
			//source and target are keywords
			links.push({ target: parseInt(n), source: parseInt(index_map[parent]) });
		}
	}	
}

parse_data();

var force = d3.layout.force()
	.size([WIDTH, HEIGHT])
	.nodes(nodes)
	.links(links);

var svg = d3.select("body").append("svg")
	.attr("width", WIDTH)
	.attr("height", HEIGHT);

var link = svg.selectAll(".link")
	.data(links)
	.enter().append("line")
	.attr("class", "link");	

var node = svg.selectAll(".node")
	.data(nodes)
	.enter().append("circle")
	.attr("class", "node");

force.on("tick", function() {	
	node
		.attr("r", WIDTH/100)
		.attr("cx", function(d) {return d.x})
		.attr("cy", function(d) {return d.y});

	link
		.attr("x1", function(d) {return d.source.x})
		.attr("y1", function(d) {return d.source.y})
		.attr("x2", function(d) {return d.target.x})
		.attr("y2", function(d) {return d.target.y});
});

force.start();



























