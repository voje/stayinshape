#!/bin/bash

# ./append_prepend.sh words.txt krizanke.txt

words="$1"
wordlist="$2"

outname="prep_app_list.txt"
touch "$outname"

for word in $(cat "$words"); do
    for line in $(cat "$wordlist"); do
        if [[ "${#line}" -ge 8 ]]; then
            echo "$line" >> "$outname"
        fi 
        tmp="$word$line"
        if [[ "${#tmp}" -ge 8 ]]; then
            echo "$word$line" >> "$outname"
            echo "$line$word" >> "$outname"
        fi
    done
done

