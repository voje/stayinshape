#include <iostream>

using namespace std;

int main() {
    int arr[] = {1,2,3,4,5};
    int len = sizeof(arr) / sizeof(*arr);
    int j = len - 1;
    int *new_arr = (int *) malloc (len);
    for (int i = 0; i <= j; i++) {
        new_arr[2*i] = arr[i];
        new_arr[2*i + 1] = arr[j];
        j--;
    }

    for (int i = 0; i < len; i++) {
        cout << new_arr[i] << ", ";
    }
}