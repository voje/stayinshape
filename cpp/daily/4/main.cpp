#include <iostream>

using namespace std;

int main() {
    int input[] = {1,7,-3,5,-7,9,5,1,2};
    size_t ln = sizeof(input) / sizeof(*input);
    bool *p_mask = new bool[ln];
    fill(p_mask, p_mask + ln * sizeof(*p_mask), false);

    for (size_t i=0; i < ln; i++) {
        int n = input[i];
        if (n > 0 && n <= ln) {
            p_mask[n-1] = true;
        }
    }

    for (size_t i=0; i < ln; i++) {
        // cout << p_mask[i] << endl;
        if (!p_mask[i]) {
            cout << "Missing number: " << i + 1 << endl;
            break;
        }
    }

    return 0;
}