#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

/*
// I give up... c++ and functional programming don't go together well.
// Going to look into Functors instead.

auto cnst(int a, int b) {
    auto testprint = [a,b](string (&f)(int a, int b)) { return f(a, b); };
    return testprint;
}

auto get_a(auto (&f)(string (&f)())) {
    auto _return_a = [](int a, int b) { return a; };
    return _return_a;
}
*/

template <typename T>
ostream &operator<<(ostream &out, vector<T> &v) {
    // some stackOverflow knowledge:
    /*
    Choose auto x when you want to work with copies.
    Choose auto &x when you want to work with original items and may modify them.
    Choose auto const &x when you want to work with original items and will not modify them.
     */
    out << "[ ";
    for ( auto const &x : v) {
        cout << x << ", ";
    }
    out << " ]";
    return out;
}

int main() {
    // exercise:
    // here are two different vectors (int and char)
    // make a generic sort function

    int N = 10;
    vector<int> vint;
    for (int i=0; i<N; i++) {
        vint.push_back(rand() % 100);
    }
    vector<char> vchar;
    for (int i=0; i<N; i++) {
        vchar.push_back((char)(65 + rand() % 26));
    }

    sort(vint.begin(), vint.end());
    cout << vint << endl;

    sort(vchar.begin(), vchar.end());
    cout << vchar << endl;

    return 0;
}