#include <iostream>
#include <vector>

using namespace std;

void with_division(vector<int> *in, vector<int> *out) {
    int prod = 1;
    for (int el: *in) {
        prod *= el;
    }
    for (int el: *in) {
        (*out).push_back((int)(prod / el));
    }
}

int main() {
    vector<int> v = {1,2,3,4,5};

    vector<int> answ;
    with_division(&v, &answ);

    for (int el: answ) {
        cout << el << ", ";
    }
    cout << endl;

    return 0;
}