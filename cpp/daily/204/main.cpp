#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <sstream>

using namespace std;

struct node {
	char name;
	node *parent = nullptr;
	node *lchild = nullptr;
	node *rchild = nullptr;
};

string serialize(node *n) {
	if (n == nullptr) {
		return ".";
	}
	ostringstream oss;
	oss << n->name << "(";
	oss << serialize(n->lchild);
	oss << serialize(n->rchild);
	oss << ")";
	return oss.str();
}

node* deserialize(string &st, int &str_idx) {
	char c = st.at(str_idx);
	if (c == '.') return nullptr;
	node *nn = new node();
	nn->name = c;
	str_idx++;  // on '('	

	str_idx++;  // on '('	
	nn->lchild = deserialize(st, str_idx);

	str_idx++;  // on '('	
	nn->rchild = deserialize(st, str_idx);

	str_idx++; // on ')'
	return nn;
}

void add_child(node *parent, node *child, char lr) {
	child->parent = parent;
	switch (lr) {
		case 'l':
			parent->lchild = child; break;
		case 'r':
			parent->rchild = child; break;
	}
}

void dfs(node *tree) {
	if (tree == nullptr) {
		cout << ". ";
		return;
	}
	cout << tree->name << " ";
	dfs(tree->lchild);
	dfs(tree->rchild);
}

void bfs(node *tree) {
	vector<node*> gen = {tree};
	while (gen.size() > 0) {
		vector<node*> newgen;
		for (int i=0; i< gen.size(); i++) {
			cout << gen.at(i)->name << " | ";
			if (gen.at(i)->lchild != NULL)
				newgen.push_back(gen.at(i)->lchild);
			if (gen.at(i)->rchild != NULL)
				newgen.push_back(gen.at(i)->rchild);
		}
		cout << endl;
		gen = newgen;
	}
}

int n_nodes(node *tree) {
	int depth = 0;
	while (tree != NULL) {
		depth ++;
		tree = tree->lchild;	
	}
	return int(pow(2, depth) - 1);
}

node* build_tree() {
	node *A = new node();
	A->name = 'A';

	node *B = new node();
	B->name = 'B';
	add_child(A, B, 'l');

	node *C = new node();
	C->name = 'C';
	add_child(B, C, 'l');

	node *D = new node();
	D->name = 'D';
	add_child(B, D, 'r');

	node *E = new node();
	E->name = 'E';
	add_child(A, E, 'r');

	node *F = new node();
	F->name = 'F';
	add_child(E, F, 'r');

	return A;
}

int main() {
	node *p_root;
	p_root = build_tree();

	cout << "dfs:" << endl;
	dfs(p_root);
	cout << endl;

	cout << "bfs:" << endl;
	bfs(p_root);

	cout << "N nodes: " << n_nodes(p_root) << endl;

	cout << "Serializing..." << endl;
	string serialized = serialize(p_root);
	cout << serialized << endl;

	cout << "Deserializing..." << endl;
	int str_idx = 0;	
	node *p_newroot = deserialize(serialized, str_idx);

	cout << "old: " << serialize(p_root) << endl;	
	cout << "new: " << serialize(p_newroot) << endl;	

	cout << "testing serialization and deserialization: strings match: ";
	cout << (serialize(p_root)).compare(serialize(p_newroot)) << endl;
}