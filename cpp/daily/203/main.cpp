#include <iostream>
#include <climits>
#include "../tests/vars.h"

using namespace std;

int min (vector<int> *v) {
    int min = (*v).at(0);
    int prev = INT_MIN;
    for (int el: *v) {
        // desc jump
        if (el < prev) {
            return (el < min) ? el : min;
        }
        prev = el;
    }
}

int main() {
    Vars vars;
    cout << "The answer is: " << vars.test << endl;
    cout << min(&vars.vec1) << endl;
}