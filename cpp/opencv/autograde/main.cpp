#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <stdexcept>

using namespace std;
using namespace cv;

struct Config{
	string img_file;
};

const int EXPECTED_N_HORIZONTAL = 51;
const uint WHITE = 255;
const uint BLACK = 0;
const uint GRAY = 170;

ostream &operator<<(ostream &os, tuple<int,int,int> &tpl) {
	os << "[ ";
	os << get<0>(tpl) << ", ";
	os << get<1>(tpl) << ", ";
	os << get<2>(tpl);
	os << " ]";
	return os;
}

template <typename T>
ostream &operator<<(ostream &os, vector<T> &v) {
	os << "[ ";
	for (const auto &x : v) {
		os << x << ", ";
	}
	os << "]";
	return os;
} 

int flood(int i, int j, Mat mat, int flooded) {
	mat.at<uchar>(i, j) = GRAY;
	while (i < mat.rows - 1) {
		while (j < mat.cols - 1) {
			if (mat.at<uchar>(i + 1, j) == WHITE) {
				flooded = flood(i + 1, j, mat, flooded + 1);
			} else if (mat.at<uchar>(i, j + 1) == WHITE) {
				flooded = flood(i, j + 1, mat, flooded + 1);
			}
			else return flooded;
		}
	}
	return flooded;
}

bool _lines_sort(Vec4i &l1, Vec4i &l2) { return l1[1] < l2[1]; }
bool _blobs_sort(tuple<int,int,int> &t1, tuple<int,int,int> &t2) {
	return get<2>(t1) < get<2>(t2);
}

vector<Vec4i>* find_horizontal_lines(Mat &base) {
	Mat element = getStructuringElement(
		MORPH_CROSS,
	    Size(3, 3),
		Point(1, 1)
	);
	dilate( base, base, element);
	element = getStructuringElement(
		MORPH_RECT,
	    Size(3, 3),
		Point(1, 1)
	);
	erode(base, base, element);

	vector<Vec4i> lines; // will hold the results of the detection
	// HoughLinesP(base, lines, 1, CV_PI/2, 100, 200, 1); // Finds nice vertical lines
	HoughLinesP(base, lines, 1, CV_PI/2, 100, 200, 100); // finds both vertical an horizontal lines
	// lines[i] = [col_1, row_1, col_2, row_2]; row_1 == row_2 for horizontal lines

	if (lines.size() == 0) {
		cout << "Found 0 lines." << endl;
		throw;
	}

	// Debug - show lines.
	/*
	Mat shwlines;
	cvtColor(base, shwlines, COLOR_GRAY2BGR);
	for( size_t i = 0; i < lines.size(); i++ ){
		Vec4i ln = lines[i];
		cout << ln << endl;
		line(shwlines,
		    Point(ln[0], ln[1]),
		    Point(ln[2], ln[3]),
		    Scalar(0,0,255), 3, 8);
	}
	imshow("Display", shwlines);
	waitKey(0);
	*/

	cout << "Extracting horizontal lines..." << endl;
	vector<Vec4i> *p_horizontal = new(vector<Vec4i>);

	sort(lines.begin(), lines.end(), _lines_sort);
	Vec4i prev_line = {-1, -1, -1, -1};
	for( const Vec4i &ln : lines ) {
		// skip if not horizontal
		if (ln[1] != ln[3])
			continue;
		
		// init first horizontal line
		if (prev_line[0] == -1) {
			prev_line = ln;
			p_horizontal->push_back(ln);
			continue;
		}

		// skip clustered lines
		if (ln[1] - prev_line[1] <= 1)
			continue;
		
		p_horizontal->push_back(ln);
		prev_line = ln; 
	}

	return p_horizontal;
}

vector<int> process_poll_row(Mat &pr) {
	int N_CELLS = 11;
	int WHITES_THRESHOLD = 25;  //getting rid of false positives in cell pixels

	double cell_len = (double)pr.cols / (double)N_CELLS;
	// cout << cell_len << endl;

	vector<int> res;
	// int max_whites = -1;
	// int max_i = -1;
	for (int i = 1; i < N_CELLS; i++) {

		// some fine tunning, to get the cell without borders
		Range rows(0, pr.rows);
		Range cols(
			(int)round(i * cell_len) + 11, 
			(int)round((i + 1) * cell_len) - 1
		);
		Mat cell(pr, rows, cols);

		int whites = countNonZero(cell);
		// cout << whites << ", ";
		/*
		if (
			whites > WHITES_THRESHOLD &&
			whites > max_whites
		) {
		imshow("Display", cell);
		waitKey(0);
		*/
		if (whites > WHITES_THRESHOLD) {
			res.push_back(i);
		}

	}
	cout << "Displaying: " << res << endl;

	imshow("Display", pr);
	waitKey(0);

	return res;
}

int main() {
	cout << "OpenCV version: " << CV_VERSION << endl;

	// Read config
	Config config;
	ifstream f_config ("../config.txt");
	if (f_config.is_open()) {
		string line;
		while (getline(f_config, line)) {
			config.img_file = line;
		}
	} else {
		cout << "can't open config.txt" << endl;
		return 1;
	}

	// Read image
	Mat src;
	src = imread(config.img_file, IMREAD_GRAYSCALE);
	if (src.empty()) {
		cout << "Failed reading image." << endl;
		return 1;
	}

	// Prepare display
	namedWindow("Display.", WINDOW_AUTOSIZE);

	Mat small_orig, small;
	pyrDown(src, small_orig, Size(round(src.cols / 2), round(src.rows / 2)));

	// perform morphology on small
	adaptiveThreshold(small_orig, small, WHITE, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 11, 2);

	// we want the original inverted too
	bitwise_not(small_orig, small_orig);	

	// Morphological transformations
	Mat element = getStructuringElement(
		MORPH_CROSS,
	    Size(3, 3),
		Point(1, 1)
	);
	dilate( small, small, element );
	erode( small, small, element );

	// find biggest blob
	Mat fldmat;
	fldmat = small.clone();	
	vector<tuple<int, int, int>> cnd;  // candidates
	int px;
	for (int i = 0; i < fldmat.rows; i++) {
		for (int j = 0; j < fldmat.cols; j++) {
			px = fldmat.at<uchar>(i, j);
			// cout << px << endl;

			if (px == WHITE) {
				int fld;
				fld = flood(i, j, fldmat, 0);
				tuple <int,int,int> tp;
				tp = make_tuple(i, j, fld);
				cnd.push_back(tp);
			}
		}
	}

	sort(cnd.begin(), cnd.end(), _blobs_sort);
	/*
	for (auto &el : cnd)  {
		cout << el << endl;
	}
	*/

	Mat tmp_blob, left_blob, right_blob;
	tuple<int,int,int> tmp_px, left_px, right_px;

	tmp_px = cnd[cnd.size() - 1];
	left_px = cnd[cnd.size() - 2];
	// cout << tmp_px << ", " << left_px << endl;
	if (get<1>(tmp_px) < get<1>(left_px)) {
		right_px = left_px;
		left_px = tmp_px;
	} else {
		right_px = tmp_px;
	}

	cout << "Detecting largest blobs:" << endl;
	cout << "left_px: " << left_px << endl;
	cout << "right_px: " << left_px << endl;

	tmp_blob = small.clone();
	flood(get<0>(left_px), get<1>(left_px), tmp_blob, 0);
	left_blob = (tmp_blob == GRAY);

	tmp_blob = small.clone();
	flood(get<0>(right_px), get<1>(right_px), tmp_blob, 0);
	right_blob = (tmp_blob == GRAY);

	/*
	cout << left_blob.size() << endl;
	imshow("Display", left_blob);
	waitKey(0);
	cout << right_blob.size() << endl;
	imshow("Display", right_blob);
	waitKey(0);
	return 0;
	*/

	int poll_row_idx = 0;
	vector<Mat*> largest_blobs {&left_blob, &right_blob};
	for (int i=0; i < largest_blobs.size(); i++) {
		vector<Vec4i> horizontal = *(find_horizontal_lines(*(largest_blobs[i])));

		cout << "N horizontal lines: " << horizontal.size() << endl;
		if (horizontal.size() != EXPECTED_N_HORIZONTAL && false) {
			cout << "HoughLinesP found incorrect number of horizontal lines." << endl;
			throw;
		}

		int leftmost_px = 50000;
		int rightmost_px = -1;
		for (Vec4i const &line : horizontal) {
			if (line[0] < leftmost_px) leftmost_px = line[0];
			if (line[2] > rightmost_px) rightmost_px = line[2];
		}

		// get poll_rows using horizontal lines
		Range cols(leftmost_px, rightmost_px);
		for( size_t i = 1; i < horizontal.size(); i++) {
			Vec4i prev_line = horizontal[i-1];
			Vec4i curr_line = horizontal[i];

			// 2 px buffer, to get rid of the horizontal lines
		    Range rows(prev_line[1] + 2, curr_line[1] - 2);
		    Mat poll_row(small_orig, rows, cols);

		   	vector<int> res = process_poll_row(poll_row);
			cout << "poll_row[" << poll_row_idx << "]: " << res << endl;
			poll_row_idx++;

		    // imshow("Display", poll_row);
		    // waitKey(0);	
		}
	}

	return 0;
}
