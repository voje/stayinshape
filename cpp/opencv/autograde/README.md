# gocv

## X11 setup
Since gocv is a very visual library, we will want to see the results.  
On the host machine, allow docker to access X11:
```bash
$ xhost +local:docker
```
Use the X11 mounts, used in Makefile (`$ make run`).  

You can test it by installing `x11-apps` and running `$ xeyes`.  

## Makefile
The makefile runs a container with gocv and an X-server connection (you can open windows and display stuff).  

## C++ opencv
```bash
$ g++ main.cpp -I /usr/local/include/opencv4
```

## Build
Use CMake:
```bash
mkdir build
cd build
cmake ..
make
```

## Todo
* Split code into classes.
* Fix flooding (scan3)
* Scan 2 returns no lines. (angle?)

