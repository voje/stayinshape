#include <iostream>
#include "levenshtein.cpp"
#include <cstdio>
#include <cstdlib>
#include <errno.h>

int main(int argc, char* argv[]) {
	if (argc == 1) {
		std::cout << "Input: word1, word2.\nOutput: levenshtein distance." << std::endl;
		return 1;
	} else if (argc != 3) {
		errno = 1;
		perror("Invalid number of arguments.");
		exit(1);
	}

	//0 rec
	//1 ite
	//2 both
	Levenshtein lvst(2);
	//std::cout << "method: " << lvst.get_method() << std::endl;

	int res;
	//res = lvst.calc_distance("kitten", "sitting");
	res = lvst.calc_distance(argv[1], argv[2]);

	std::cout << res << std::endl;

	return 0;
}
