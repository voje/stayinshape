
class Levenshtein {
public:
	Levenshtein( int );
	int get_method();
	int calc_distance( std::string, std::string );
	int calc_distance_rec( std::string, size_t, std::string s2, size_t );
	int calc_distance_ite( std::string, size_t, std::string, size_t );
	int calc_distance_both( std::string, size_t, std::string, size_t );
private:
	int method;	
};