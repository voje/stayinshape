#include "levenshtein.h"

#define NDEBUG

Levenshtein::Levenshtein(int method) {
	this->method = method;
}

int Levenshtein::get_method() {
	return this->method;
}

int Levenshtein::calc_distance(std::string s1, std::string s2) {
	if (this->method == 0) {
		return calc_distance_rec(s1, s1.length(), s2, s2.length());
	} else if (this->method == 1) {
		return calc_distance_ite(s1, s1.length(), s2, s2.length());
	} else if (this->method == 2) {
		return calc_distance_both(s1, s1.length(), s2, s2.length());
	} else {
		return -1;
	}
}

int Levenshtein::calc_distance_rec(std::string s, size_t len_s, std::string t, size_t len_t) {
	if (len_s == 0) return len_t;
	if (len_t == 0) return len_s;

	int cost;
	if (s[len_s - 1] == t[len_t - 1])
		cost = 0;
	else
		cost = 1;

	//todo... build c++ array and find minimum with iterator
	int min;
	int min1;
	min = (this->calc_distance_rec(s, len_s-1, t, len_t) + 1);
	min1 = (this->calc_distance_rec(s, len_s, t, len_t-1) + 1);
	if (min1 < min)
		min = min1;
	min1 = (this->calc_distance_rec(s, len_s-1, t, len_t-1) + cost);
	if (min1 < min)
		min = min1;

	return min;
}

int min_val(int a, int b, int c) {
	//std::cout << a << " " << b << " " << c << std::endl;
	if (a < b) {
		if (a < c) {
			return a;
		}
		return c;
	}
	else if (b < c) {
		return b;
	}
	return c;
}

int Levenshtein::calc_distance_ite(std::string s, size_t len_s, std::string t, size_t len_t) {
	/*
	# # t e s t (t)
	# 0 1 2 3 4
	t 1 0 1 2 2
	s 2 1 1 1 2
	e 3 2 1 2 2
	t 4 2 2 2 2
        (s)
	*/

        int mat[len_s+1][len_t+1] = {0};

        for (int i=0; i<=len_t; i++) {
            mat[0][i] = i;
        }
        for (int j=0; j<=len_s; j++) {
            mat[j][0] = j;
        }

        for (int i=0; i<len_s; i++) {
            for (int j=0; j<len_t; j++) {
                int mod = 0;
                if ( t[j] != s[i] ) {
                    mod = 1;
                    #ifndef NDEBUG
                    std::cout << "t[i]: " << t[i] << ", s[j]: " << s[j] << ", mod: " << mod << std::endl;
                    #endif
                }
                int ii = i+1;
                int jj = j+1;
                mat[ii][jj] = min_val(mat[ii-1][jj] + 1, mat[ii][jj-1] + 1, mat[ii-1][jj-1] + mod);
            }
        }

        #ifndef NDEBUG
        //print mat
        for (int i=0; i<=len_s; i++) {
            for (int j=0; j<=len_t; j++) {
                std::cout << mat[i][j] << ", ";
            }
            std::cout << std::endl;
        }
        #endif

        return mat[len_s][len_t];
}

int Levenshtein::calc_distance_both(std::string s, size_t len_s, std::string t, size_t len_t) {
	int res1, res2;
	res1 = calc_distance_rec(s, len_s, t, len_t);
        res2 = calc_distance_ite(s, len_s, t, len_t);
        //std::cout << res1 << ", " << res2 << std::endl;
	if (res1 == res2) {
		return res1;
	} else {
		return -2;
	}
}
