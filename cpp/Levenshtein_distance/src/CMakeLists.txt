cmake_minimum_required (VERSION 2.0)
project (Levenshtein_distance)

include_directories (levenshtein)

set (SOURCE_FILES main.cpp)

add_executable (main ${SOURCE_FILES})