#pragma once

#include <iostream>
#include "Object.h"

namespace rpg {
    class Object_visitor {
    public:
        void visit(rpg::Object& obj) {
            std::cout << "[v] " << obj.name << std::endl;
        }
    };
}