#include "Map.h"
#include <sstream>
#include <iostream>
#include <string>
#include <cstdlib>

void rpg::Map::add_object(Object &obj) {
	objects.push_back(obj);	
}

void rpg::Map::tick() {
	for (Object &o: objects) {
		o.act();
	}
}

void rpg::Map::draw() {
	std::string out(width * height, '+');
	for (const Object& ob : objects) {
		out.at(ob.y * width + ob.x) = (char)ob.name[0];
	}
	for (int i=0; i<height; i++) {
		std::string tmpout = out.substr(i * width, width);
		std::cout << tmpout << std::endl;
	}
}
