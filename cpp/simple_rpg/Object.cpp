#include "Object.h"
#include <iostream>

void rpg::Object::act() {
	std::cout << name << "(" << x << ", " << y << ") making a move." << std::endl;
}

void rpg::Object::accept_visitor(rpg::Object_visitor& ov) {
	ov.visit(*this);
}