#include "Tile.h"
#include <stdexcept>

void rpg::Tile::obj_enter(rpg::Object *eobj) {
    if (obj != nullptr) {
        throw std::runtime_error("Tile already contains an object.");
    }
    obj = eobj;
}

rpg::Object* rpg::Tile::obj_exit() {
    if (obj == nullptr) {
        throw std::runtime_error("Attempting to get object from empty tile.");
    }
    rpg::Object* rt = obj;
    obj = nullptr;
    return rt;
}