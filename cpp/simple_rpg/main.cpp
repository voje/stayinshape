//#include "Map.h"
#include <string>
#include <vector>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include "time.h"
#include "Field.h"
#include "Tile.h"

int main() {
	//rpg::Map m{20, 10};

	std::srand(time(NULL));

	rpg::Field<10, 20> fld;

	std::vector<rpg::Object*> objects;
	std::vector<std::string> names = std::vector<std::string>{
		"Archer", "Bruiser", "Cudgel", "Destroyer"};
	for (auto s : names) {
		rpg::Object* ob = new rpg::Object{s};
		objects.push_back(ob);

		int x,y;
		do {
			x = std::rand() % (fld.width());
			y = std::rand() % (fld.height());
		} while (!fld.free(y, x));

		fld.place(ob, y, x);
	}

	fld.draw();

	std::cout << std::endl;

	rpg::Object_visitor objv;
	for (auto obj : objects) {
		obj->accept_visitor(objv);
	}

	return 0;
}