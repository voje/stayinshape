#pragma once

#include <iostream>
#include "Tile.h"

namespace rpg {

    template <int h, int w>
    class Field {
    private:
        rpg::Tile* field[h][w];
    public:
        Field<h, w>();
        int height() { return h; };
        int width() { return w; };
        void draw();
        void place(rpg::Object*, int, int);
        bool free(int x, int y) {
            return (field[x][y]->obj_show()) == nullptr;
        }
    };

}

template <int h, int w>
rpg::Field<h, w>::Field() {
    for(int i=0; i<h; ++i) {
        for (int j=0; j<w; ++j) {
            field[i][j] = new rpg::Tile{};
        }
    }
}

template <int h, int w>
void rpg::Field<h, w>::draw() {
    for (int i=0; i<h; ++i) {
        for (int j=0; j<w; ++j) {
            if (!free(i, j)) {
                std::cout << field[i][j]->obj->initial();
            } else {
                std::cout << "+";
            }
        }
        std::cout << std::endl;
    }
}

template <int h, int w>
void rpg::Field<h, w>::place(rpg::Object* obj, int y, int x) {
    field[y][x]->obj_enter(obj);
}