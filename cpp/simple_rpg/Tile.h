#pragma once

#include "Object.h"

namespace rpg {
    class Tile {
    private:
    public:
        rpg::Object *obj;
        Tile() {
            obj = nullptr;
        };
        enum type {
            WALL,
            FLOOR
        };
        void obj_enter(rpg::Object *eobj);
        rpg::Object* obj_exit();
        const rpg::Object* obj_show() const {
            return obj;
        };
        
    };
}