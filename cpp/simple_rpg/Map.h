#pragma once

#include "Object.h"
#include <vector>

namespace rpg {

/** This is an obsolete map class. */
class Map {
private:
	std::vector<rpg::Object> objects;
public:
	int width;
	int height;
	Map(int x, int y): width(x), height(y) {};
	void add_object(Object&);
	void tick();
	void draw();
};

}