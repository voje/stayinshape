#pragma once

#include <string>
#include <iostream>

namespace rpg {

	class Object_visitor;

	class Object {
	private:
	public:
		std::string name;
		int x;
		int y;
		Object(std::string& s): name(s) {};
		void act();
		std::string get_name() { return name; };
		const char initial() const { return name[0]; };
		void accept_visitor(rpg::Object_visitor& ov);
	};

	class Object_visitor {
	public:
		void visit(Object& ob) {
			std::cout << "[v]" << ob.name << std::endl;
		}
	};
}
