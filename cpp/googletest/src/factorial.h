namespace factorial {

/**
 * @brief Calculate factorial of n.
 * 
 * @param n Calculate factorial of n.
 * @return int Factorial of n.
 */
int factorial(int n);

}