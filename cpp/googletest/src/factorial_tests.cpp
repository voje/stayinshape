#include <gtest/gtest.h>

#include <factorial.h>

namespace {

TEST(Factorial, Negative) {
    EXPECT_EQ(factorial::factorial(0), 1);
    EXPECT_EQ(factorial::factorial(-1), 1);
    EXPECT_EQ(factorial::factorial(-2), 1);
    EXPECT_EQ(factorial::factorial(-3), 1);
    EXPECT_EQ(factorial::factorial(-42), 1);
}

}