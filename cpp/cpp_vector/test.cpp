#include <sstream>
#include <vector>
#include <iostream>

int main(int argc, char **argv) {
  std::vector<int> vec;
  std::stringstream ss;

  ss << "test" << 42;
  
  for (int i=0; i<10; i++) {
    vec.push_back(i); 
  }  

  std::cout << ss.str() <<std::endl;

  return 0;
}
