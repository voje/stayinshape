#!/bin/bash

destinations=(
    www.google.com
    www.youtube.com
    www.ddasafsdfdfasfasdfa.sss
)

spinner=('-' '\' '|' '/')

i=0
while true; do
    res=""
    res+="[ ${spinner[i%4]} ]\n\n"
    i=$i+1

    for dest in ${destinations[@]}; do
        ping -c 1 $dest 1>/dev/null 2>&1
        res+="[ $? ] ${dest}\n"
    done

    clear
    echo -e $res
    sleep 1
done

