#!/bin/bash

echo "Password: "
read PASS

if [ $# -eq 0 ]; then
  FOLDER=./secret
else
  FOLDER=$1
fi

for file in $(find $FOLDER); do
  if [ -f $file ] && [[ ! $file == *.gpg ]]; then
    echo "Encrypting " $file
    gpg --batch --yes -c --passphrase ${PASS} $file
  fi
done

# decrypt
for file in $(find $FOLDER); do
  if [ -f $file ] && [[ $file == *.gpg ]]; then
    outfile=${file%.gpg}
    echo "Decrypting " $file " --> " $outfile
      gpg --batch -d --passphrase ${PASS} $file 2>/dev/null 1> $outfile
  fi
done

