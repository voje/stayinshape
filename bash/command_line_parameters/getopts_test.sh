#!/bin/bash

#
# Testing getopts.
#

# Prepend : to disable default error handling (illegal options) and implement your own (\? case).
optstring=":n:tf:"

# getopts <predefined-options> <insert-option-here> <parse-options-from-this-string>
# Also gets set: $OPTARG, when parsing an option ENDING with : (option with an argument.

testargs="-n 3 -t -f 'test' -a"  # normally, you would pass $@ instead of this test string.

echo "Parsing test arguments:"
echo "$testargs"
echo

while getopts $optstring opt $testargs; do
    case ${opt} in
        n)
            ;;
        t)
            ;;
        f)
            ;;
        \?)
            echo "Undefined option $opt"
            ;;
    esac
    echo "Read option: $opt"
    echo "Found arg: $OPTARG"
    echo
done