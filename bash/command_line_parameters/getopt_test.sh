#!/bin/bash

#
# getopt test
#

#
# long arg doesn't work.. use getopts instead
#

# getopt adds -- to the end of arguments.

optstring=":n:tf:"

testargs="-n 3 -t -f 'test' --longarg test"  # normally, you would pass $* instead of this test string.

args=$(getopt $optstring $testargs)

set -- $args

while [[ $# -gt 0 ]]; do

    echo $1

    case $1 in
        -n)
            echo "Got n"
            echo "Arg: $2"
            shift 2
            ;;
        -t)
            echo "Got t"
            shift 1
            ;;
        -f)
            echo "Got f"
            echo "Arg: $2"
            shift 2
            ;;
        --longarg)
            echo "Got longarg"
            echo "Arg: $2"
            shift 2
            ;;
        *)
            echo "Unknown argument."
            shift 1
            ;;
    esac

done
