#!/bin/bash

function fetch_files () {
    for item in *; do
        if [[ -d "$item" ]]; then
            cd "$item"
                fetch_files
            cd ..
        elif [[ "$item" =~ .*.mp3 ]]; then
            filepaths+=("$(pwd)/${item}")
        fi
    done
}

function w_fetch_files () {
    local tmp_loc="$(pwd)"
    cd "$1"
    fetch_files
    cd "$tmp_loc"
}

function main () {
    local filepaths=()
    w_fetch_files "/home/kristjan/Music"

    while true; do
        N="${#filepaths[@]}"
        n=$(( RANDOM % N ))
        file="${filepaths[$n]}"
        mpg123 -q "$file"
        echo "[*]"
        sleep 10
    done
}

main

