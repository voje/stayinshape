#!/bin/bash
script_path="/home/kristjan/git/stayinshape/bash/personal_assistant"
cd "$script_path"

NHRS=8
if [[ ${#@} -gt 0 ]]; then
    NHRS=$1
fi

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo."
    echo "sudo $0 $*"
    exit 1
fi

MINUTE=60
HOUR=$(( 60 * $MINUTE ))
M=$(( 30 * $MINUTE ))
N=$(( $NHRS * $HOUR - $M ))

# wait M seconds
echo "${M} seconds until sleep."
echo "Alarm in $NHRS hours."
sleep $M

# send computer to sleep for N seconds
rtcwake -m mem -s $N

echo "$(pwd)"
# quote time
./random_quote.sh

# play some morning tunes
./random_song.sh

