#!/bin/bash

URL="https://www.brainyquote.com/link/quotebr.rss"

data=$(curl -s "$URL")
# echo "$data" > tmp.html
# data=$(cat tmp.html)

TEMP=$(mktemp)

# Authors
echo "$data" \
    | awk -F "<item>" '/<title>.*<\/title>/' \
    | sed -e 's/<\/*title>//g' \
    | tail -n +3 \
    > "$TEMP"
authors=()
while read line; do
    authors+=("$line")
done < "$TEMP"

# Quotes
echo "$data" \
    | awk -F "<item>" '/<description>.*<\/description>/' \
    | sed -e 's/<\/*description>//g' \
    | tail -n +3 \
    > "$TEMP"
quotes=()
while read line; do
    quotes+=("$line")
done < "$TEMP"

N="${#authors[@]}"
n=$(( RANDOM % N ))

printf "\n\n\n\n\t%s\n\n\n\n\n\n\t%s\n\n\n\n" "${quotes[$n]}" "${authors[$n]}"

