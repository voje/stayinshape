#!/bin/bash

err=42

# Test, throw error out of a block
{
exit 1
err=$?
echo Afterblock $err
} | xargs -I{} echo {}

echo Block exit status: $?

echo Trapper ERR: $err

exit $err
