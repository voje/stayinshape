#!/bin/bash

manual="
# Go to www.hribi.net. 
# Select a route you like. 
# The page has some photo thumbnails at the bottom. 
# Paste the page url as an argument to this script. 
# It will download the source photos. 
# Save photos to phone. 
# Go hiking, don't worry about signal. 
"
if [[ "$1" == "-h" || "$1" == "--help" ]]; then
    echo "$manual"
    exit 0
fi

test_url="http://www.hribi.net/izlet/ljubelj_vrtaca_/11/78/28"
base_url="www.hribi.net"
#url="$test_url"
url=$1
if [[ -z $url ]]; then
    echo "Insert hribi.net URL."
    echo "Example: ${test_url}"
    exit 1
fi

site=$(curl "$url")
reg_patt="slika.asp\?pot=[0-9]+"
arr_hrefs=(
    $(echo "$site" | grep -o -E "$reg_patt" --color=auto)
)

# echo ${#arr_hrefs[@]}
file_basename=$(date +%s)
dirname="hribi_slike"
mkdir $dirname

i=0
for href in ${arr_hrefs[@]}; do
    suburl=$base_url/$href
    subsite=$(curl $suburl)
    reg_patt="slike1/[a-Z,0-9]*.jpg"
    img_href=$(echo "$subsite" | grep -o -E "$reg_patt" | head -n 1)
    img_url=$base_url/$img_href
    path="./${dirname}/${file_basename}_${i}.jpg"
    curl $img_url > $path
    echo "Created $path."
    i=$((i+1))
done

