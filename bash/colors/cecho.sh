#!/bin/bash
# 
# author:   Kristjan Voje 
# desc:     Simple echo function for colorful output. 
#           Do not use it on strings with parentheses. 
# 
#==================================================000

function cecho() {
    # echo with color parsing
    # r() red
    # g() green
    # y() yellow
    echo -e $@ | \
        sed "s/r(\(.*\))/`printf "\e[31m"`\1`printf "\e[0m"`/g" | \
        sed "s/g(\([^)]*\))/`printf "\e[32m"`\1`printf "\e[0m"`/g" | \
        sed "s/y(\([^)]*\))/`printf "\e[33m"`\1`printf "\e[0m"`/g"
}

cecho "Da vidimo, ali g(ta zadeva) dejansko r(deluje)."
cecho "------------"
cecho "g(I be green.)"
cecho "r(I'm red.)"
cecho "y(Go, Pikachu.)"
cecho "------------"

