#!/bin/bash

TMP=$(mktemp)
DATA="./data.txt"

while IFS= read -r line; do
    echo "$line" >> $TMP 
done < "$DATA"

while read line; do
    echo "$line"
done < "$TMP" | shuf

rm "$TMP"

