# Install a VM
Using virsh, virt-install.

## Task
Install ubuntu server, set it up and clone it.
No networking for now.

## Needed packages and tools

* virt-install
`$ apt-get install virtinst`

## Serial console
We need to enable serial console on guest in order to use `virsh console <guest>`.
```
$ sudo systemctl enable serial-getty@ttyS0.service
$ sudo systemctl start serial-getty@ttyS0.service
```

