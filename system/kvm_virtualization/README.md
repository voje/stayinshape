# Summary of useful commands

Following [this guide](http://alexander.holbreich.org/qemu-kvm-introduction/).

## Setup
Set up kvm with the right user permissions.
```bash
# Check if cpu is kvm compatible (vmx for Intel, svm for AMD).
$ egrep -c "vmx|svm" /proc/cpuinfo
# If you get a 0, try enablink virtualization in BIOS.

# Check if KVM is switched ON in BIOS.
$ lsmod | grep -E "virtio|kvm-intel|kvm"

# You can try turning kernel modules on with:
$ modprobe kvm
$ modprobe kvm_intel
$ modprobe virtio
# Debugging:
$ dmesg | grep kvm

# Installing kvm:
$ sudo apt-get install qemu-kvm

# Check install.
$ ls -la /dev/kvm

# Add your user to the kvm group.
$ adduser $USER kvm

# Last check:
$ id $USER
```
## Usage
There are different types of images: 

* .raw - global (default) format
* .vdi - VirtualBox format
* .qed - needs an underlying image (raw?), creates overlying image using only differences (sparse). I suppose for creating snapshots?
* .qcow2 - slower than .qed, supports multiple snapshots and AES encryption.

```bash
# Create a new image:
$ qemu-img create -f image.img 10G
$ qemu-img info image.raw

# Run a virtualmachine 
$ qemu-system-x86_64 -hda image.raw -cdrom "$path_to_iso" -boot d -m 1024

Minimal to boot up a live image:
The `-enable-kvm` flag gives a huge performance boost.
$ qemu-system-x86_64 -boot d -cdrom ../images/kali-linux-light-2018.2-amd64.iso -enable-kvm -m 1024

# TODO
Trying to run kali with a network bridge.
$ sudo qemu-system-x86_64 -boot d -cdrom ../images/kali-linux-light-2018.2-amd64.iso -device e1000,netdev=net0,mac=$macaddress -netdev tap,id=net0 -m 1024 -enable-kvm
```
## libvirt
Checking out libvirt (manager for hypervisors (in our case the hypervisor is QEMU which users the hypervisor KVM as a hardware accelerator).  

Installing on arch (virt-manager is the GUI, libvirt contains the command line virsh).
$ pacman -S libvirt virt-manager

For different networking setups.
$ pacman -S ebtables dnsmasq bridge-utils openbsd-netcat iptables firewalld


## A list of needed packages:
```bash
$ pacman -S \
    virtinst \
    virt-manager \
    kvm \
    python-libvirt \
    libvirt-bin \
    qemu \
    virt-viewer \
    bridge-utils \
```

## Bridging networks
After some tweaking, I managed to run a kali-live with a network, bridged to the ethernet interface. 
Wireless interface didn't permit enslaving. 
Sequence that did the trick: 
```bash
# create a bridge
$ ip link add br0 bridge
$ ip link set dev br0 up
# make sure the other device is up
$ ip link set dev eth0 master br0
```
You should have a working bridge now. 
Start up `virt-manager`, add a device, at network settings add a custom network device and type in the bridge name (br0). 
Start up the virtual machine. 
Profit. 

After closing the virtual machine, I needed to restart NetworkManager to use internet on host. 
`$ systemctl restart NetworkManager`


## Shell only
Trying to run a Kali vm using qemu and kvm shell commands.
Managed to run kali with NAT network:
```bash
$ qemu-system-x86_64 \
-cdrom /home/kristjan/virtual_images/kali-linux-light-2018.2-amd64.iso \
-enable-kvm \
-m 1024
```

## Using virsh

* virt-install
`$ apt-get install virtinst`

# Create a vm
Use ubuntu_server, set up ssh and basics, clone it. 

## Using virt-install
We'll be installing arch-linux.
We're sitting in a directory, containing two directories: `virtual_images`, `virtual_disks`.

```bash
# Create disk space:
$ qemu-image create -f qcow2 ./virtual_disks/arch_disk.qcow2 10G

# Install arch:
$ sudo virt-install --name arch1 --memory 2048 --vcpus 2 --cdrom ./virtual_images/archlinux-2018.12.01-x86_64.iso --network default --disk ./virtual_disks/arch_disk.qcow2
```

If we need to start over:
```bash
$ virsh destroy arch1
$ virsh undefine arch1
```















