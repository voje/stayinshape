# Routed Network
(This)[https://jamielinux.com/docs/libvirt-networking-handbook/routed-network.html] guide.

## Spin up a normal VM.
```bash
$ sudo systemctl start libvirtd
$ virsh list --all

# Problem: connecting via terminal without networking.

```

