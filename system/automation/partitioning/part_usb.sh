#!/bin/bash

######
#
# Create a NTFS usb.
#
######

echo "Make sure your device is unmounted."
echo "Device unmounted? [y/N]"
read um
if [[ "$um" != "y" ]]; then
    exit
fi

echo "Tell me the device name (usually /dev/sdb or /dev/sdc)."
read dname
if [ -z "$dname" ]; then
    echo "I need a name."
    exit
fi

if [[ "$dname" == "/dev/sda" ]]; then
    echo "/dev/sda is dangerous territory, aborting."
    exit
fi

echo "How should I name the USB?"
read lab

if [ -z "$lab" ]; then
    echo "Empty label name, aborting."
    exit
fi

echo "Unmounting."
sudo umount "$dname"

echo "Creating msdos partition table with 1 ntfs partition."
sudo parted --script "$dname" \
    mklabel msdos \
    mkpart primary ntfs 0% 100% \

echo "Formatting with NTFS. Using label: $lab"
sudo mkfs.ntfs -v -L "$lab" "${dname}1"

echo "Your USB stick is ready!"

