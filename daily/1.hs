-- let's go!!

myZip a = take (length a) (concat (map (\(a, b) -> [a, b]) (zip a (reverse a))))

-- here's a beautiful exmample from stackOverflow:
notMyZip d = take (length d) [z | (x,y) <- zip d (reverse d), z <- [x,y]]

