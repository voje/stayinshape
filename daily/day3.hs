data BinTree a = EmptyNode
  | FullNode a (BinTree a) (BinTree a)
  deriving (Eq, Ord, Read, Show)

-- had to add 'Show a =>' to make it work
serialize :: Show a => BinTree a -> String
serialize (FullNode v l r) = show v ++ ['|'] ++ serialize(l) ++ serialize(r)
serialize EmptyNode = ['|']

a = FullNode
  5
  (FullNode 3 EmptyNode EmptyNode)
  (FullNode 7 EmptyNode EmptyNode)

-- reading a stream
sa = serialize a
-- sa = "5|3|||7|||"

nextInt :: String -> (Bool, Int, String)
nextInt (x1:(x2:xs))
  | length xs == 0 = (False, -1, "")
  | x1 == '|' = (False, -1, x2:xs)
  | otherwise = (True, read [x1], xs)

-- exercise
loopString :: String -> [Int]
loopString (x)
  | (length s) == 0 = []
  | b = i : loopString s
  | otherwise = loopString s
  where (b, i, s) = nextInt x
