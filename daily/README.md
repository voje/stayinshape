# Daily coding problem
From this awesome site: (https://dailycodingproblem.com/)[link].


## 1.
Good morning. Here's your coding interview problem for today:

Given a stack of N elements, interleave the first half of the stack with the second half reversed using one other queue. 

For example, if the stack is [1, 2, 3, 4, 5], it should become [1, 5, 2, 4, 3].

If you would like to get the solution, consider subscribing! You can subscribe at https://dailycodingproblem.com/.

Are you interviewing right now? Let me know! I'm here to help.

Best,

Lawrence


## 2.
Good morning. Here's your coding interview problem for today:

Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i. Solve it without using division and in O(n).

For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24].

If you would like to get the solution, consider subscribing! You can subscribe at https://dailycodingproblem.com/.

Are you interviewing right now? Let me know! I'm here to help.

Best,

Lawrence


## 3.
Good morning. Here's your coding interview problem for today:

Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s), which deserializes the string back into the tree.

If you would like the solution, consider upgrading to Plus! You can upgrade here: https://dailycodingproblem.com/.

Best,

Lawrence


