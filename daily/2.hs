-- this is probably not O(n) and it uses division.
-- We'll need to use C++ for this one.
second :: [Integer] -> [Integer]
second a = [div (product a) x | x <- a]

-- let's user this:
-- left vector * right vector
input = [2,3,4,5]
test = [60, 40, 30, 24]

-- myMul :: [a] -> [a]
-- myMul [a, b] = [a, a * b]
-- myMul (x:xs) = [x] ++ myMul xs

addFst :: (Num a) => [a] -> [a]
addFst [] = []
addFst [a] = [a]
addFst (x:xs) = [x * (head xs)] ++ tail xs

myAcc :: (Num a) => [a] -> [a]
myAcc [] = []
myAcc [a] = [a]
myAcc (x:xs) = [x] ++ myAcc (addFst (x:xs))

a = myAcc (take (length input) (1:input))
b = reverse (myAcc (take (length input) (1:(reverse input))))

res = zipWith (*) a b

