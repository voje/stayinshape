#include <iostream>
#include <vector>

using namespace std;

void printVector(vector<int> v) {
    for (vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << *it << ", "; 
    }
    cout << endl;
}

int main() {
    vector<int> stack {1, 2, 3, 4, 5};

    vector<int> left {1};
    for (int i = 0; i < stack.size() - 1; i ++) {
        left.push_back(left.back()*stack[i]);
    }

    vector<int> right {1};
    for (int i = stack.size() -1; i > 0; i--) {
        right.push_back(right.back()*stack[i]); 
    }

    vector<int> res;
    for (int i = 0; i < left.size(); i++) {
        int tmp = left[i] * right[right.size() - 1 - i];
        res.push_back(tmp);
    }

    printVector(stack);
    printVector(left);
    printVector(right);
    printVector(res);

    return 0;
}
