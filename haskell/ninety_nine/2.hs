myButLast :: [a] -> a
myButLast [] = error "List too short."
myButLast [a] = error "List too short."
myButLast [a,b] = a
myButLast (x:xs) = myButLast xs

myButLast1 :: [a] -> a
myButLast1 [] = error "Should be at least 2 long."
myButLast1 [a] = error "Should be at least 2 long."
myButLast1 (x:xs) =
    if (length xs) == 1 then x
    else myButLast xs
