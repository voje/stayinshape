import MyData

numEl :: [a] -> Int
numEl [] = 0
numEl (x:xs) = (numEl xs) + 1
