-- runLength encoding with unhomogenous result:
-- [(a, 2), b, (c, 3), ...]
-- need a utility type

-- import our pack function and test lists
import Pack_9_10

data Element a = Multi Int a | Single a deriving (Show)

-- 11 encode
runLen :: (Eq a) => [a] -> [Element a]
runLen list = map transform (pack list) where
    transform :: [a] -> Element a
    transform (x:[]) = Single x
    transform (x:xs) = Multi (length (x:xs)) x


-- 12 decode
decode :: [Element a] -> [a]
decode list = concatMap transform list where
    transform :: Element a -> [a]
    transform (Single x) = [x]
    transform (Multi n x) = replicate n x

