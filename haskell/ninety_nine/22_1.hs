range :: Int -> Int -> [Int]
range a b = range' a b [] where
  range' :: Int -> Int -> [Int] -> [Int]
  range' a b list
   | a == b = a:list
   | otherwise = a:(range' (a+1) b list)
