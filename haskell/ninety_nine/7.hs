-- nested list type
data NList a = El a | List([NList a])

list1 = List([El 1, El 2, List([El 3, El 4])])

-- watch it with the parenthesis
-- every equation must match the upper one
flatten :: NList a -> [a]
flatten (El a) = [a]
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
flatten (List []) = []

