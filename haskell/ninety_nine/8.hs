-- eliminate consecutive elements from a list

list1 = [1,1,2,2,3,3,3,4,5,5,5,6,7,8,8]

elCon :: (Eq a) => [a] -> [a]
elCon [a] = [a]
elCon (x:xs) =
  if x == (head xs) then elCon xs
  else x : elCon(xs)
