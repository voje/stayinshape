-- split a list into 2 parts
-- argument is length of 1st part

import MyData

split :: [a] -> Int -> ([a], [a])
split list n = split' ([], list) n where
    split' :: ([a], [a]) -> Int -> ([a], [a])
    split' (ly, y:ys) n
        | n <= 0 = (ly, y:ys)
        | otherwise = split' ((ly ++ [y]), ys) (n-1)

split1 :: [a] -> Int -> ([a], [a])
split1 list n = ((take n list), (drop n list))
    
