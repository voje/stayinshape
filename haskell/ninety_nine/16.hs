-- drop every Nth element of a list

import MyData

delNth :: [a] -> Int -> [a]
delNth x n = delNth' x n n where
  delNth' [] _ _ = []
  delNth' (x:xs) n nOrig
    | n == 1 = delNth' xs nOrig nOrig
    | otherwise = x:(delNth' xs (n-1) nOrig)

-- using perlude
delNth1 :: [a] -> Int -> [a]
delNth1 [] _ = []
delNth1 x n = (take (n-1) x) ++ (delNth1 (drop n x) n)

