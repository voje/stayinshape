-- repli [1,2,3], 3
-- replicates the element in the list 3 times

import MyData

repli :: [a] -> Int -> [a]
repli [] _ = []
repli (x:xs) r = (repli' [x] r) ++ (repli xs r)
  where
    repli' :: [a] -> Int -> [a]
    repli' x r
      | (length x == r) = x
      | otherwise = repli' ((head x):x) r

