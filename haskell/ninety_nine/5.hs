import MyData

myReverse :: [a] -> [a]
myReverse [a] = [a]
myReverse (x:xs) = (myReverse xs) ++ [x]

myReverse1 :: [a] -> [a]
myReverse1 = foldl (flip (:)) []

