myLast :: [a] -> a
myLast a = last a

myLast1 :: [a] -> a
myLast1 [] = error "Takes non-empty list."
myLast1 [x] = x
myLast1 (x:xs) = myLast xs
