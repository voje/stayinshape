module MyData where

arr1 = [4,6,3,2,8,7]
arr2 = "kristjan voje"
arr3 = []

data NestedList a = El a | List [NestedList a]
nested1 = El 42
nested2 = List [El 1]
nested3 = List [List ([El 1, List ([El 2, El 3]) ]), El 5]

