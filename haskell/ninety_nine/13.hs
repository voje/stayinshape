-- import list1 list2 list3
import Pack_9_10

data Element a = Multi Int a | Single a deriving (Show)

together :: (Eq a) => [a] -> [Element a]
together list = reverse (together' [] [] list) where
    together' :: (Eq a) => [Element a] -> [a] -> [a] -> [Element a]
    together' output buffer [] = (mkEl buffer):output
    together' output [] (x:input) = together' output [x] input
    together' output (b:buffer) (x:input)
        | x == b = together' output (x:b:buffer) input
        | otherwise = together' ((mkEl (b:buffer)):output) [x] input

    mkEl :: [a] -> Element a
    mkEl [x] = Single x
    mkEl (x:xs) = Multi (length (x:xs)) x
