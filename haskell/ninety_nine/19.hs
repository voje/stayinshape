-- rotate list n places to the left (use length and ++)

import MyData

rotate :: [a] -> Int -> [a]
rotate list n = let a = take n list in (drop n list) ++ a

