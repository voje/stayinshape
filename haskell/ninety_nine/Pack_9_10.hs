module Pack_9_10 (pack, list1, list2, list3) where

list1 = [1,1,2,2,3,3,3,4,4,5,5,5,6]
list2 = [1,2,3]
list3 = "KKKRRRiiiSSttjjAAaa n n n nn n n n"

-- 9
-- pack repeated elements into subsecutive sublists
pack :: (Eq a) => [a] -> [[a]]
pack xs = reverse (pack' [[]] xs) where
    pack' :: (Eq a) => [[a]] -> [a] -> [[a]]
    pack' lys [] = lys
    pack' [[]] (x:xs) = pack' [[x]] xs
    pack' (ly:lys) (x:xs)
        | x == last ly = pack' ((x:ly):lys) xs
        | otherwise = pack' ([x]:ly:lys) xs

-- 10
runLength :: (Eq a) => [a] -> [(Int, a)]
runLength list = [((length x), (head x)) | x <- (pack list)]

