-- delete Nth element

import MyData

rmAt :: Int -> [a] -> [a]
rmAt n lst = let (a,x:xs) = splitAt (n-1) lst in a ++ xs

