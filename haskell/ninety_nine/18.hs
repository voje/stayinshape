-- slice from a list
-- arguments i, j are indices (starting with 1)

import MyData

{- this code is disgusting and it doesn't work
slice :: [a] -> Int -> Int -> [a]
slice list i j = fst (slice' ([], (snd (slice' ([], list) i))) j) where
    slice' :: ([a], [a]) -> Int -> ([a], [a])
    slice' (ly, (y:ys)) n
        | n <= 1 = (ly, (y:ys))
        | otherwise = slice' ((ly ++ [y]), ys) (n-1)
-}

--this function is beautiful and it works
slice :: [a] -> Int -> Int -> [a] 
slice xs i j = [x | (x, k) <- zip xs [1..j], i <= k]

