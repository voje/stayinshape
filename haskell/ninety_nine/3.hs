import MyData

elementAt :: [a] -> Int -> a
elementAt a b = a !! (b - 1)

elementAt1 :: [a] -> Int -> a
elementAt1 [] _ = error "Empty list."
elementAt1 (x:xs) k = 
    if k == 1 then x
    else elementAt1 xs (k-1)
