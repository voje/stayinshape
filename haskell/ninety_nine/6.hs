import MyData

myPalindrome :: (Eq a) => [a] -> Bool
myPalindrome [] = True
myPalindrome [a] = True
myPalindrome (x:xs) = (x == (last xs)) && (myPalindrome (init xs))

