-- duplicate elements of a list
-- a b c -> a a b b c c

import MyData

duplicate :: [a] -> [a]
duplicate [] = []
duplicate (x:xs) = x:(x:(duplicate xs))

-- list comprehension
duplicate1 :: [a] -> [a]
duplicate1 list = concat [[x,x] | x <- list]

