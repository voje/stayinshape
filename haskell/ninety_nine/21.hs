-- insert element into list

import MyData

insAt :: a -> [a] -> Int -> [a]
insAt a lst n = let (xs, ys) = splitAt (n-1) lst in xs ++ a:ys

