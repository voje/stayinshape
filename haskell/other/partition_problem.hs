import Data.Char

charListToInts :: [Char] -> [Int]
charListToInts a = [ord x - 48 | x <- a, not(elem x [' ', ','])]

isPartWrapper :: [Int] -> Bool
isPartWrapper arr = 
    if odd (sum arr) || length arr < 2 then
        False
    else
        isPart arr (div (sum arr) 2)

isPart :: [Int] -> Int -> Bool
isPart arr k = 
    if k == 0 then
        False
    else if k < 0 || k > sum arr then
        True
    else
        (isPart (init arr) k) || (isPart (init arr) (k - (last arr)))


main = do
    let sample = "1,2,3,4,5"
    putStrLn "Enter an array of integers."
    putStrLn ("for example: " ++ sample)
    putStrLn (show (charListToInts sample))
    line <- getLine
    putStrLn (show (isPartWrapper (charListToInts line)))
