# Let's dip our toes in Haskell :D

## Installation
On Manjaro, I had to install: `ghc`, then fix some dependency issues by installing 'ncurses'.  

## Basic usage
Save functions in a file (`file.hs`).  
Fire up `$ghci` in the same folder.  
Load the functions with `:l file`.  

## Stuff to remember
* If statements are expressions. Mandatory else, always return a value.
* Functions start with a lowercase letter.  

## Unordered notes
let keyword  ```> let x = 3```  
```l1 ++ l1``` concatenate lists. Haskell has to iterate over left one  
```el : li``` prependes element to list  
```li !! n``` gets n-th element out of a list (starts with 0)  
```head tail init last``` blows up if a list is empty (not checked at compile time!!!)  
```length null``` null tells if a list is empty - use it!
```reverse take drop```  
```minimum maximum sum product elem```  
ranges ```[1..20]```, [firstelement, secondelement..upperlimit] ```[1,3..30]```  
infinite lists are layz-evaluated ```take 30 [2,4..]```  
```cycle repeat replicate```  
list comprehensions, like in python ```[x*2 | x <- [1..10], x*2 >= 12]```


## Types
:t to get type  
polymorphic functions have type variables (any type -> any type)  
Types: Capital letters, type variables: lower case letters  
myfun :: Int -> Int  


## TypeClass
Sort of like GoLang interface? TypeClass for example Eq means all Types under this typeclass can be compared by the == which has a signature (Eq a) => a -> a -> bool, which means that it takes an Eq TypeClass variable a.  
Read typeclass means the function `read` can take an argument of that TypeClass and turn it into a variable. (string to var, like `"[a, b, c]"` to an actual array.
We can tell the compiler which Type we want after the expression:  
`read "4" :: Int`  
`let x = read "[1,2,3,4,5,6]" :: [Int]`  


## IO actions
IO is separated from the rest of the language. Since haskell functions are pure (they don't change program state), 
functions like getLine and putStrLn have special status. 
`name <- getLine` works like this: getLine returns a IO String type and the <- operator "unboxes" it and stores it into name as a String.  


## Exercises:
# todo..



