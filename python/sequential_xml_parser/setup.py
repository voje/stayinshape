from setuptools import setup

setup(
    name="seqparser",
    version="0.1",
    description="for parsing sskj2 html soup",
    author="voje",
    author_email="kristjan.voje@gmail.com",
    licence="MIT",
    packages=["seqparser"],
    install_requires=["bs4"],
    zip_save=False,
)
