from bs4 import BeautifulSoup as BS
import random
import re
from collections import defaultdict
from time import time
import pickle
import json
from copy import deepcopy as DC

# Match sese ordinals (1., 2., ...)
rord = re.compile(r"^ *[0-9]+\. *$")

# Get rid of accented characters.
intab = "ÁÉÍÓÚàáäçèéêìíîñòóôöùúüčŔŕ"
outtb = "AEIOUaaaceeeiiinoooouuučRr"
transtab = str.maketrans(intab, outtb)

class Parser:
    def __init__(self):
        pass

    def parse_file(self, path, f_parse_line):
        tstart = time()
        entries = defaultdict(list)
        with open(path, "r") as f:
            for line in f:
                data = f_parse_line(line)
                if data is not None:
                    entries[data["izt_clean"]].append(data)
        print("parse_file({}) in {:.2f}s".format(path, time() - tstart))
        return entries

    def parse_line(self, line):
        data = {
            "izt": "",
            "izt_clean": "",
            "senses": defaultdict(list)
        }
        soup = BS(line, "html.parser")

        current_sense_id = "no_id"
        for span in soup.find_all("span"):

            # sense id
            if span.string is not None:
                rmatch = rord.match(span.string)
                if rmatch is not None:
                    current_sense_id = rmatch.group().strip()

            title = span.attrs.get("title")
            if title is not None:
                # only verbs and adjectives
                if "glagol" in title.lower():
                    data["bv"] = "G"
                    data["bv_full"] = title
                elif "pridevnik" in title.lower():
                    data["bv"] = "P"
                    data["bv_full"] = title

                # žšč
                if title == "Iztočnica":
                    data["izt"] = span.string
                    data["izt_clean"] = span.string.translate(transtab).lower()

                # sense description
                if title == "Razlaga":
                    data["senses"][current_sense_id].append(
                        ("razl", span.string))

                if title == "Sopomenka":
                    subspan = span.find_all("a")[0]
                    data["senses"][current_sense_id].append(
                        ("sopo", subspan.string))

        # save verbs and adjectives
        if (
            ("bv" not in data) or
            (data["bv"] != "P" and data["bv"] != "G")
        ):
            return None

        # sanity check
        if data["bv"] == "P" and " se" in data["izt_clean"]:
            print(data)
            exit(1)

        # append _ to adjective keywords
        if data["bv"] == "P":
            data["izt_clean"] = data["izt_clean"] + "_"

        #cleanup
        del(data["bv"])
        del(data["bv_full"])

        return data

    def read_and_pickle(self):
        file_path = "../data/sskj2_v1.html"
        entries = dict(self.parse_file(file_path, self.parse_line))
        print("entries len: " + str(len(entries)))
        with open("tmp.pickle", "wb") as f:
            tmpstr = json.dumps(dict(entries))
            pickle.dump(tmpstr, f)

    def load_pickle(self):
        with open("tmp.pickle", "rb") as f:
            tmpstr = pickle.load(f)
            return json.loads(tmpstr)

    def helper_loop(self, data, fnc):
        for k, lst in data.items():
            for el in lst:
                fnc(el)

    def gen_se_list(self, data):

        def fnc1(el):
            ic = el["izt_clean"]
            if " se" in ic:
                se_list.append(ic)

        def fnc2(el):
            ic = el["izt_clean"]
            if ic in se_pruned:
                se_pruned.remove(ic)

        # hw entries that only exist with " se"
        se_list = []
        self.helper_loop(data, fnc1)
        se_pruned = set([hw.split(" se")[0] for hw in se_list])
        self.helper_loop(data, fnc2)
        return sorted(list(se_pruned))

    def remove_se(self, data):

        def fnc1(el):
            nel = DC(el)
            ic = nel["izt_clean"]
            if " se" in ic:
                nic = ic.split(" se")[0]
                nel["izt_clean"] = nic
            data_new[nel["izt_clean"]].append(nel)

        data_new = defaultdict(list)
        self.helper_loop(data, fnc1)
        return dict(data_new)

    def reorganize(self, data, se_list):
        # some hw entries have several headwords,
        # some senses have subsenses
        # index everything, make 1 object per hw
        data_new = {}
        for k, lst in data.items():
            new_el = {
                "hw": k,
                "has_se": k in se_list,
                "senses": []
            }

            # if there is a single hw entry, hw_id is 0
            if len(lst) == 1:
                homonym_id = -1
            else:
                homonym_id = 0

            # loop homonyms
            for el in lst:
                homonym_id += 1
                # loop top lvl sense ids
                for sense_id, sens_lst in el["senses"].items():
                    # loop subsenses
                    for i, sens in enumerate(sens_lst):
                        nsid = sense_id.split(".")[0]
                        if len(sens_lst) > 1:
                            nsid += ("." + str(i + 1))
                        new_sense = {
                            "homonym_id": homonym_id,
                            "sense_id": nsid,
                            "sense_type": sens[0],
                            "sense_desc": sens[1],
                        }
                        new_el["senses"].append(new_sense)
            hw = new_el["hw"]
            if hw in data_new:
                print("Shouldn't be here.")
                print(new_el)
                exit(1)
            data_new[hw] = DC(new_el)
        return data_new


def plst(lst):
    for el in lst:
        print(el)

if __name__ == "__main__":
    p = Parser()

    # p.read_and_pickle()
    # exit()

    data = p.load_pickle()
    print("data len: " + str(len(data)))
    se_list = p.gen_se_list(data)
    print("se_list len: " + str(len(se_list)))
    data1 = p.remove_se(data)
    data2 = p.reorganize(data1, se_list)
    print("data2 len: " + str(len(data2.keys())))
