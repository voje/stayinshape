#!/usr/bin/python

import socket
import threading
import signal
import sys
from time import time

ADDR = "localhost"
PORT = 8000
sockets = []


def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    for s in sockets:
        print("Closing...")
        s.close()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)


def myrecv(sock):
    alldata = ""
    while True:
        data = sock.recv(1024)
        if not data:
            break
        alldata += data.decode()
    return alldata


def single_connection():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockets.append(sock)
    sock.connect((ADDR, PORT))
    req = "GET / HTTP/1.1 \r\n"
    sock.sendall(req)
    tstart = time()
    while True:
        elapsed = int(time() - tstart)
        if elapsed > 10:
            break
    sock.sendall("\r\n")
    re = myrecv(sock)
    print(re)
    sock.close()

"""
for i in range(1):
    thr = threading.Thread(target=single_connection)
    thr.start()
"""

single_connection()

