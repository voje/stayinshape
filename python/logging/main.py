import logging
from module1.script1 import func1
from pathlib import Path

logpath = Path("test.log").absolute()
logpath.touch(exist_ok=True)
logpath.resolve()  # /one/two/three/../four --> /one/two/four

logging.basicConfig(filename=str(logpath), level=logging.INFO)
logging.info('Started')
func1()
logging.info('Finished')
