# Algorithm for generating crossword puzzles.
Language: python3

## Stuff I've learned about python3
* python3 doesn't need \_\_init\_\_.py files,
* python3's open() converts to unicode if you ommit the 'b' flag,
* you should use the unicode sandwich when writing python scripts.
