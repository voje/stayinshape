#!/usr/bin/env/python
# -*- coding: utf-8 -*-

import csv
from Classes.Grid import Grid

class Crossw:

    #words = [] #this would be a static member

    def __init__(self):
        print('Initializing class Crossw.')
        self.words = [] #this is a private member

    def read_csv(self, filepath):
        # unicode sandwich: convert to and use unicode internally 
        # (python3 does thi automatically if you don't use open('rb')
        with open(filepath, 'r') as csvfile:
            print("Reading words from %s." % (filepath))
            reader = csv.reader(csvfile, delimiter='\n')
            for word in reader:
                self.words += [word[0].lower()]

        #also sort by length
        self.words.sort(key=len, reverse=True)

    def create_grid(self):
        g = Grid()
        for w in self.words:
            g.add_word_opt(w)
        #debugging
        g.print_grid()
        return g

    def print_words(self):
        print ("words:\n-------")
        ##print (self.words)
        print ('\n'.join(self.words))
        print ()


