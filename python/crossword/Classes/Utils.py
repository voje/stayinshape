#!/usr/bin/env/python
# -*- coding: utf-8 -*-

def tuple_add(t1, t2):
    return (t1[0]+t2[0], t1[1]+t2[1])

def tuple_scale(t1, scalar):
    return (t1[0]*scalar, t1[1]*scalar)
