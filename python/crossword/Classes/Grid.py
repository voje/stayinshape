#!/usr/bin/evn/python
# -*- coding: utf-8 -*-

from Classes.Utils import *

class Grid:

    rotations = [(0,1), (1,1), (1,0), (1,-1), (0,-1), (-1,-1), (-1,0), (-1,1)]

    #implement a resizable grid:
    #hashmap (i,j) tuples as keys, (char, count) as values
    #angles are compass angles

    #todo: each grid cell needs a list of words it belongs to
    
    def __init__(self):
        #key = tuple(i, j), val = [tuple(word, angle), ..]
        self.words = {}
        #key = tuple, val = (char, char_count, list of words it belongs to)
        self.grid = {}
        #top left and bottom right edge of the grid
        self.tl = (0,0)
        self.br = (0,0)

    def update_size(self, tup):
        if tup[0] < self.tl[0]:
            self.tl = (tup[0], self.tl[1])
        elif tup[0] > self.br[0]:
            self.br = (tup[0], self.br[1])
        if tup[1] < self.tl[1]:
            self.tl = (self.tl[0], tup[1])
        elif tup[1] > self.br[1]:
            self.br = (self.br[0], tup[1])
        
    def add_word(self, root, angle, word):
        #call eval_adding_word to check if addition is valid
        #root = tuple(i, j)
        #angle = tuple(+-1, +-1)
        self.words[word] = (root, angle)
        for i,char in enumerate(word):
            loc = tuple_add(root, tuple_scale(angle, i))
            if loc in self.grid:
                if self.grid[loc][0] != word[i]:
                    return -1 #remove this
                #increment char count if char exists
                old = self.grid.pop(loc)
                self.grid[loc] = (old[0], old[1]+1, old[2] + [word])
            else:
                #else init with 1
                self.grid[loc] = (word[i], 1, [word])
        #upbade grid size based on start and end of word
        self.update_size(root)
        self.update_size(loc)
        return 0

    def eval_adding_word(self, root, angle, word):
        #count intersections (-1 if we chars mismatch)
        #if added word overlaps a wird with the same directoin,
        #return -2
        score = 0
        for i,char in enumerate(word):
            loc = tuple_add(root, tuple_scale(angle, i))
            if loc in self.grid:
                #check for mismatching chras
                if self.grid[loc][0] != word[i]:
                    return -1
                #check for same directoin words
                for w in self.grid[loc][2]:
                    w_angle = self.words[w][1]
                    if w_angle == angle:
                        return -2
                #else, add intersection score
                score = score + 1
        return score

    def add_word_opt(self, word):
        #tries all positions and locations, add word to best one
        best_root = (9999, 9999)
        best_angle = (9999, 9999)
        best_eval = -1;
        for i in range(self.tl[0], self.br[0]+1):
            for j in range(self.tl[1], self.br[1]+1):
                root = (i,j)
                for ang in Grid.rotations:
                    tmp_eval = self.eval_adding_word(root, ang, word) 
                    if tmp_eval > best_eval:
                        best_root = root
                        best_angle = ang
                        best_eval = tmp_eval
        if best_eval == -1:
            #todo "improve me"
            #extend the grid (add a row)
            best_root = (self.br[0]+1, self.tl[1])
            best_angle = (0, 1)
        self.add_word(best_root, best_angle, word)

    def rm_word(self, word):
        if word not in self.words:
            return -1
        root = self.words[word][0]
        angle = self.words[word][1]
        del self.words[word]
        for i,char in enumerate(word):
            loc = tuple_add(root, tuple_scale(angle, i))
            if loc in self.grid:
                if self.grid[loc][1] <= 1:
                    del self.grid[loc]
                else:
                    old = self.grid.pop(loc)
                    tmp_list = old[2].remove(word)
                    self.grid[loc] = (old[0], old[1]-1, tmp_list)
        return 0

    def print_grid(self, counts=False):
        for i in range(self.tl[0], self.br[0]+1):
            for j in range(self.tl[1], self.br[1]+1):
                #print cell
                if (i,j) in self.grid:
                    print( "  %c" % (self.grid[(i,j)][0]), end="" )
                    if counts:
                        print( ",%d" % (self.grid[(i,j)][1]), end="" )
                else:
                    print( "  %c" % ('#'), end="" )
                    if counts:
                        print( ",%d" % (0), end="" )
            print()
        print()

if __name__ == "__main__":
    print("Test: ")
    g = Grid()

    g.add_word((0,0), (0,1), "eagle")
    #g.add_word((0,1), (-1,-1), "antilope")
    #g.print_grid()

    #g.rm_word("antilope")
    #g.print_grid()

    g.add_word_opt("lettuce")
    g.add_word_opt("iris")
    g.add_word_opt("best")
    g.add_word_opt("češnja")
    g.print_grid()



















