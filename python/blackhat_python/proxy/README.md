# Simple proxy server

Test: proxy a loopback http server to LAN.
```bash
$ echo "hello" > index.html
$ python3 -m http.server --bind 127.0.0.1 8001
```

In another terminal:
# Proxy will listen on `0.0.0.0:8003` and tunnel the connection to `127.0.0.1:8001`.
$ ./proxy1.py
```

Take a machine on the LAN, open up the broser and search `http://<proxy host ip>:8003`.

