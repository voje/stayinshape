#!/usr/bin/python3
import socket

def proxy(IN_HOST, IN_PORT, OUT_HOST, OUT_PORT):
    insock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    insock.bind((IN_HOST, IN_PORT))
    insock.listen(1)

    wait_for_connections = True
    while  wait_for_connections:
        inconn, inaddr = insock.accept()

        outsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        outsock.connect((OUT_HOST, OUT_PORT))
        while True:
            indata = inconn.recv(1024)
            if not indata:
                break
            if str(indata) == "exit":
                break
            print("\n\n[[ {}:{} =({}:{})=> {}:{} ]]".format(
                inaddr[0], inaddr[1], IN_HOST, IN_PORT, OUT_HOST, OUT_PORT))
            print(indata)
            outsock.sendall(indata)
            while True:
                reply = outsock.recv(1024)
                if not reply:
                    break
                print("\n[[ {}:{} =({}:{})=> {}:{} ]]".format(
                    OUT_HOST, OUT_PORT, IN_HOST, IN_PORT, inaddr[0], inaddr[1]))
                print(reply)
                inconn.send(reply)

        inconn.close()
        outsock.close()

if __name__ == "__main__":
    print("Starting python3 proxy.")

    IN_HOST = "0.0.0.0"
    IN_PORT = 8081
    OUT_HOST = "127.0.0.1"
    OUT_PORT = 8080

    proxy(IN_HOST, IN_PORT, OUT_HOST, OUT_PORT)

