import socket

hostname = '127.0.0.1'
port = 9999

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect((hostname, port))

# Newlines matter
msg1 = """GET / HTTP/1.1
Host: google.com

"""
msg2 = "GET / HTTP/1.1\r\nHost: google.com\r\n\r\n"

msg3 = "TEST"

client.send(msg3.encode())

resp = client.recv(4096)

print(resp)

