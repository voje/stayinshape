import sys
import getopt
import socket

target = ""
port = 0
listen = False

def usage():
    print("""
    BTC netcat
    usage:
    bt_netcat.py -t target_host -p por  BTC netcat
    usage:
    bt_netcat.py -t target_host -p port
    """)
    sys.exit(1)


def main():
    global target
    global port
    global listen

    if not len(sys.argv[1:]):
        usage()

    try:
        optlist, args = getopt.getopt(sys.argv[1:], "t:p:")
        # print(optlist)  # tuples ('-t', 'argument')
        # print(args)     # list of argument arguments
        # Yo dawg, I saw you had an arg s I put an arg in your arg.

    except getopt.GetoptError as err:
        print(err)
        usage()

    print(optlist)
    for o,a in optlist:
        if o in ("-t", "--target"):
            target = a
        elif o in ("-p", "--port"):
            port = int(a)

    if not listen and len(target) and port > 0:
        buffer = input("<-- ")
        # CTRL+D (EOF) to cancel
        client_sender(buffer)

    if listen:
        # server_loop()
        print("TODO")

    
def client_sender(buffer):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        print("Connecting to: {}:{}".format(target, port))
        client.connect((target, port))

        if len(buffer):
            client.send(buffer.encode())

        # Some hosts send before receiving:

        # while:
        #   recv
        #   send
        while True:
            resp_len = 1
            response = ""

            while(resp_len):
                resp = client.recv(4096)
                resp_len = len(resp)

                response += resp.decode()

                if resp_len < 4096:
                    break

            print("--> " + response)

            
            buffer = input("\n<-- ")
            buffer += "\n"
            msg = "test1"
            client.send(msg.encode())
            print("sent111")

    except socket.error as err:
        print(err)
        client.close()



if __name__ == "__main__":
    main()





















