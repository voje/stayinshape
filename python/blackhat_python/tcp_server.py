import socket
import threading

host = "0.0.0.0"
# Either the deafult route to the internet or
# all IPv4 addresses on a machine.
port = 9999

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind((host, port))

# Argument is query size.
server.listen(5)

print(" * Listening on {}:{}".format(host, port))

def handle_client(client_socket):
    request = client_socket.recv(1024)

    print(" ** Received: {}.".format(request))

    msg = "ACK!"
    client_socket.send(msg.encode())
    
    client_socket.close()
    
while True:
    
    client_socket, addr = server.accept()

    print(" * Accepted connection from {}.".format(addr))

    client_handler = threading.Thread(target=handle_client, args=(client_socket,))
    client_handler.start()

