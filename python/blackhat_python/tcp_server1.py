import socket
import threading
import random

# TCP server that keeps connection until you send 'disconnect'
host = "0.0.0.0"

# Linux needs some time to kill the socket. 
port = 9990 + random.randint(0, 9)  

# Send messages with:
# $ netcat 127.0.0.1 9999

intable = "abcdefghijklmnopqrstuvwxyz"
outtabl = "4b[D3fgHiJk1mn0PqR5tuvwxy7"
transtab = str.maketrans(intable, outtabl)

def handle_client(client, addr):

    while True:
        # Read from client
        buff_len = 1
        message = ""
        while buff_len:
            buff = client.recv(4096)
            message += buff.decode()
            buff_len = len(buff)

            if buff_len < 4096:
                break

        # Sending simple newline (enter) will close.
        if len(message) > 1:
            print("[ recv ]: {}".format(message))

            response = "[#] " + message.translate(transtab)
            client.send(response.encode())
        else:
            print("[*] Closing connection with {}.".format(addr))
            client.close()
            return



if __name__ == "__main__":
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((host, port))
    server.listen(5)
    print("[*] Listening on {}:{}.".format(host, port))

    while True:
        client_socket, addr = server.accept()
        print("[*] Accepted connection from {}.".format(addr))

        client_handler = threading.Thread(target=handle_client, args=(client_socket, addr))
        client_handler.start()

