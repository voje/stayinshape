import pandas
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import locale

# Bar chart bar width.
wdth = 0.3

# For converting strings to floats
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')


def to_float(x):
    if pandas.isnull(x):
        return x
    return locale.atof(x[1:])


# Read the data.
data = pandas.read_csv(
    './financno_stanje_2017.csv',
    header=3,
    names=['date', 'name', 'up', 'down'],
    usecols=[0, 1, 2, 3]
)

# Remuve NaN rows
data = data[(~pandas.isnull(data)).any(axis=1)]

# Reindex
data = data.reset_index(drop=True)

# €123,123.123 to float
data['up'] = data['up'].apply(to_float)
data['down'] = data['down'].apply(to_float)
data['down'] = data['down'].apply(lambda x: -x)

runy = [0]
runx = [0]
# First, plot the running sum:
for index, row in data.iterrows():
    if pandas.isnull(row['up']):
        val = row['down']
    else:
        val = row['up']

    runy.append(runy[-1])
    runx.append(index)

    runy.append(runy[-1] + val)
    runx.append(index)

plt.figure(1)
plt.subplot(111)
plt.fill_between(runx, 0, runy, color='mediumspringgreen')

# Now let's draw bar plot for income and outcome.
# First, create a mask for income rows.
mask = pandas.isnull(data['up'])

yup = data['up'][~mask]
xup = data.index[~mask]

ydown = data['down'][mask]
xdown = data.index[mask]

plt.bar(xup, yup, color='blue', width=wdth)
plt.bar(xdown, ydown, color='red', width=wdth)

# Add x labels.
plt.xticks(data.index, data['name'], rotation='vertical')

plt.grid()
plt.tight_layout()  # Auto-padding for labels etc.

# Increase y tick frequency.
plt.gca().yaxis.set_major_locator(ticker.MultipleLocator(500))

# Title etc
plt.title("MEPZ Crescendo 2017")
plt.ylabel("[eur]")

print(data)
plt.show()

