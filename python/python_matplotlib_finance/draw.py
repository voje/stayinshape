
# import pandas
import random
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt

xx = []
yy = []

for i in range(0, 10):
    xx.append(i)
    yy.append(random.randint(-5, 5))

xx = np.array(xx)
yy = np.array(yy)

labels = [
    'zero', 'one', 'two', 'three', 'four', 'five',
    'six', 'seven', 'eight', 'nine', 'ten'
]

areax = [0]
areay = [0]
for i in range(len(yy)):
    areay.append(areay[-1])
    areax.append(i)

    areay.append(areay[-1] + yy[i])
    areax.append(i)

plt.figure(1)
ax = plt.subplot(111)

ax.fill_between(areax, 0, areay, color='mediumspringgreen')

# Barplot for >=0 and <0
yy = ma.masked_where(yy > 0, yy)
ax.bar(xx[yy.mask], yy[yy.mask], color='b', width=0.2)
ax.bar(xx[~yy.mask], yy[~yy.mask], color='r', width=0.2)

plt.xticks(xx, labels, rotation='vertical')
plt.show()
