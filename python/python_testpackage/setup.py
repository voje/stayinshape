from setuptools import setup

setup(name='python_testpackage',
      version='0.1',
      description='Local testpackage.',
      url='http://github.com/derp/derp.com',
      author='KV',
      author_email='kv@kv.com',
      license='KIT',
      packages=['python_testpackage'],
      install_requires=['markdown', ],
      zip_safe=False)
