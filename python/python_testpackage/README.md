## Python test package ##

Create a python package with dependencies.

### Procedure ###
There's a minimal directory structure:

name
..name
....__init__.py
..setup.py

The top level name is the name of the git directory, inner name is the actual module.

All the code will be in `__init__.py` although we should put bigger projects into separate files.
__init__.py
```python
def hello():
    return("Hello from python_testpackage.")

```

Package names should contain lower case letters. 
Module names should contain lower case letters and underscores. 
```setup.py
from setuptools import setup

setup(name='python_testpackage',
      version='0.1',
      description='Local testpackage.',
      url='http://github.com/derp/derp.com',
      author='KV',
      author_email='kv@kv.com',
      license='KIT',
      packages=['python_testpackage'],
      install_requires=[
      	'markdown',
      ],
      zip_safe=False)

```

After creating the files, go into root directory (toplevel name) and execute command:
`$ pip install .`.
Use `-e` flag to install the package with symbolic links (code edits take effect immediately).
Use `--user` flag if you're not in virtualenv and want to install a user specific package.  

You might need sudo previleges. If the package installs correctly, you can test it by opening a new python interpreter and importing the package:
```python
import python_testpackage
python_testpackage.hello()
```

### Dependencies ###
Add `install_requires` to `setup.py`.  
In this example, I required the `markdown` package. When running `pip install .`, the installer checked and installed markdown before installing python_testpackage.  



