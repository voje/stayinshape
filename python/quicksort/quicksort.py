def printLst(lst, si, ei):
    while si <= ei:
        e = lst[si]
        print "[%2d] %d" % (si, e)
        si+=1
    print 

def quicksort(lst):
    #1 element list
    if (len(lst)<=1):
        return lst
    #select pivot
    pivot = lst[-1]
    larr = []
    rarr = []
    i = 0
    while i < len(lst)-1: #exclude pivot
        if lst[i] > pivot:
            rarr.insert(0, lst[i])
        elif lst[i] < pivot:
            larr.append(lst[i])
        i+=1
    return quicksort(larr) + [pivot] + quicksort(rarr)

if __name__ == "__main__":

    a = [3,1,2,5,7,6,4]

    print a

    a = quicksort(a)

    print a

#len(arr)
#python passes functions "by reference"... you are modifying the original array!


