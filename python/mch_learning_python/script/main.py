#!/usr/bin/python

import os
import sys
import scipy
import numpy
import matplotlib
import matplotlib.pyplot as plt
import sklearn 
from sklearn import model_selection
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
import pandas

print ("""Imported :
        sys: {}
        scipy: {}
        numpy: {}
        matplotlib: {}
        sklearn: {}
        pandas: {}.""".format( sys.version, scipy.__version__, numpy.__version__, matplotlib.__version__, sklearn.__version__, pandas.__version__  ) )

script_path = os.path.realpath(__file__)
print ("Script path: {}.".format(script_path))

data_file_name = "iris.csv"
data_path = '/'.join( script_path.split('/')[0:-2] + ["data/{}".format(data_file_name)] )
print ("Data path: {}.".format(data_path))

if __name__ == "__main__":
    print ("Starting script: __main__.")
    
    attr_names = ["sepal-length", "sepal-width", "petal-length", "petal-width", "class"]
    dataset = pandas.read_csv(data_path, names=attr_names)

    print ("Dimensions of the data: {}.".format(dataset.shape))

    print ("Head:\n {}".format(dataset.head(5)))

    print ("Summary:\n {}".format(dataset.describe()))

    print ("Classes:\n {}".format(dataset.groupby("class").size()))

    """
    print ("Plotting: ")

    # Unvariate plot

    # Boxplot
    dataset.plot(kind="box", subplots=True, layout=(2,2), sharey=False, sharex=False)
    matplotlib.pyplot.show()

    # Histograms
    dataset.hist()
    matplotlib.pyplot.show()


    # Multivariate plot

    # Scatterplot
    pandas.plotting.scatter_matrix(dataset)
    matplotlib.pyplot.show()
    """

    # Split the data
    val = dataset.values
    X = val[:,0:4]
    Y = val[:,4]
    validation_size = 0.20 
    seed = 7
    X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)

"""
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
"""

models = []

from sklearn.linear_model import LogisticRegression
models += [("LR", LogisticRegression())]

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
models += [("LDA", LinearDiscriminantAnalysis())]

from sklearn.tree import DecisionTreeClassifier
models += [("CART", DecisionTreeClassifier())]

from sklearn.neighbors import KNeighborsClassifier
models += [("KNN", KNeighborsClassifier())]

from sklearn.naive_bayes import GaussianNB
models += [("NB", GaussianNB())]

from sklearn.svm import SVC
models += [("SVM", SVC())]

results = []
for name,model in models:
    k_fold = model_selection.KFold(10, random_state=7)
    cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=k_fold, scoring="accuracy")
    results += [[cv_results]] 
    print ("{:20s}: {:10f}, {}\n".format(name, cv_results.mean(), cv_results.std()))

fig = plt.figure()
fig.suptitle("My_title")
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels([x[0] for x in models])
plt.show()

# Predict with best one
svm = SVC()
svm.fit(X_train, Y_train)
pred = svm.predict(X_validation)
print ("{}\n{}\n{}".format(accuracy_score(Y_validation, pred), confusion_matrix(Y_validation, pred), 
    classification_report(Y_validation, pred)))
















