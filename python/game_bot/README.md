# Plan
I want to make a general purpose library for making video game bots.

## Compoments:

* capture window image (xdotool system calls + pyscreenshot)
* process it with opencv (object detection)
* input commands (xdotool system calls)

## Opencv setup
<del>Looks like there's no opencv package for python3.</del>  
Nope, there definetly is one. 
I created a docker container with python3 and python3-pip. 
Then I installed openCV (cv2) for python3: 
```bash
$ python3 -m pip install opencv-python opencv-contrib-python
```

## Object detection
Implementing YOLO, following this [blog post](https://www.arunponnusamy.com/yolo-object-detection-opencv-python.html).

