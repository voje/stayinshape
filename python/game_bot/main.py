#!/usr/bin/python3

import pyscreenshot as py_ss
import PIL
import cv2
import numpy
import subprocess
import argparse
import time
import re
import os

re_pos = re.compile(b"\d+,\d+")
re_siz = re.compile(b"\d+x\d+")

def get_window_coords(wname):
    # requires xdotool installed (linux)
    # windows: todo ...
    # returns (x1, y1, x2, y2) or None
    try:
        bstr = (subprocess.check_output(
        ["xdotool", "search", "--name", wname, "getwindowgeometry"],
        stderr=None,
        shell=False,
        ))
        pos_str = re_pos.search(bstr).group(0).decode("utf-8")
        x1 = int(pos_str.split(",")[0])
        y1 = int(pos_str.split(",")[1])
        siz_str = re_siz.search(bstr).group(0).decode("utf-8")
        x2 = x1 + int(siz_str.split("x")[0])
        y2 = y1 + int(siz_str.split("x")[1])
        ret = (x1,y1,x2,y2)
        print("Captured position of '{}'. Do not move the window.".format(
            wname))
        print("Target window needs to be fully wisible (captures doesn't work if window is minimized or obscured")
        print(ret)
        return ret
    except subprocess.CalledProcessError as e:
        return None


def capture(wname, sleep_ms):
    base_filename = "./capture/" + str(int(time.time())) + "_"
    i = 0
    while True:
        coords = get_window_coords(wname)
        if not coords:
            break
        im = py_ss.grab(bbox=coords)
        fname = base_filename + str(i) + ".jpeg"
        print("captured {}".format(fname))
        im.save(fname, "JPEG")

        i += 1
        sleep_s = 0.001 * sleep_ms
        time.sleep(sleep_s)


def pil_to_cv(pil_img):
    np_arr = numpy.array(pil_img)
    cv_img = np_arr[:, :, ::-1].copy() 
    return cv_img

def draw_bounding_box(image, label, color, box):
    x = round(box[0])
    y = round(box[1])
    x1 = round(box[2])
    y1 = round(box[3])
    cv2.rectangle(image, (x, y), (x1, y1), color, 2)
    cv2.putText(image, label, (x-10, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


class YOLOObjDetector:
    def __init__(self):
        self.weights_file = "./yolov3.weights"
        self.config_file = "./yolo.cfg"
        self.classes_file = "./yolo.txt"

        self.classes = None
        with open(self.classes_file, "r") as f:
            self.classes = [line.strip() for line in f.readlines()]
        self.COLORS = numpy.random.uniform(0, 255, size=(len(self.classes), 3))

        self.net = cv2.dnn.readNet(self.weights_file, self.config_file)

        # unconnected_outlayer_names (here's where we get our scores)
        # pass the layer names to forward()
        layer_names = self.net.getLayerNames()
        self.uon = [layer_names[x[0]-1] for x in self.net.getUnconnectedOutLayers()]
    
    def forward_image(self, cv_image, scale):
        # good scale = 0.01
        width = cv_image.shape[1]
        height = cv_image.shape[0]
        blob = cv2.dnn.blobFromImage(
            cv_image, scale, (416, 416), (0,0,0), swapRB=True, crop=False)

        # compute (forward blob once through net)
        self.net.setInput(blob)
        outs = self.net.forward(self.uon)

        # save hits and bounding box coordinates
        hits = []
        for out in outs:
            for detection in out:
                # detection has lengt 85
                # drop first 5, remaining 80 are class scores
                scores = detection[5:]
                class_id = numpy.argmax(scores)
                max_score = scores[class_id]
                if max_score > 0.1:
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    hits.append({
                        "class_id": class_id,
                        "box_edges": (x, y, x+w, y+h)
                    })
        print("[*] Found {} objects.".format(len(hits)))

        # draw boxes
        for hit in hits:
            class_id = hit["class_id"]
            label = self.classes[class_id]
            color = self.COLORS[class_id]
            draw_bounding_box(cv_image, label, color, hit["box_edges"])

        # display image
        cv2.imshow("boxes", cv_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


if __name__ == "__main__":

    # Command line args
    ap = argparse.ArgumentParser(description="ArgParser la di da...")
    ap.add_argument("-cap" ,"--capture", action="store_true", help="Step-by-step processing; capture screenshots. Set --interval (default 10 000 ms).")
    ap.add_argument("--interval", type=int, default=30000, help="Screen capture interval in seconds.")
    ap.add_argument("--wait", type=int, default=5, help="Wait before program starts in seconds.")
    ap.add_argument("--img_file", type=str, help="Path to image file.")
    ap.add_argument("--img_folder", type=str, help="Path to images folder (for multiple images).")
    ap.add_argument("--scale", type=float, default=0.05, help="scale parameter for dnn (fine tuning)")
    args = ap.parse_args()
    print("[*] Using arguments: {}".format(str(args)))

    # Wait for user to set up the desktop.
    print("[*] {}s wait. Prepare the game window. Make it unobscured.".format(args.wait))
    time.sleep(args.wait)
    print("[*] Starting bot.")

    if args.capture:
        capture("World of Warcraft", sleep_ms=args.interval)
        exit()

    yolo = YOLOObjDetector()

    if args.img_file is not None:
        pil_img = PIL.Image.open(args.img_file)
        yolo.forward_image(pil_to_cv(pil_img), args.scale)
    elif args.img_folder is not None:
        for img_file in os.listdir(args.img_folder):
            pil_img = PIL.Image.open(args.img_folder + "/" + img_file)
            yolo.forward_image(pil_to_cv(pil_img), args.scale)













