# game_bot
Playing around with python3, opencv. 

Need a docker container that communicates with host's XServer. 
Here are the steps from 0:

```bash
# You will need an image with python3, vim, x11-apps
# Build with Dockerfile in this folder.
$ docker build -t my_ubuntu:latest .

# create a container
$ docker run -ti --name=game_bot --net=host \
--volume=$HOME/.Xauthority:/root/.Xauthority:rw \
--volume=$HOME/git/stayinshape/python/game_bot:/root/game_bot:rw \
--env=DISPLAY \
--env=QT_X11_NO_MITSHM=1 \
my_ubuntu /bin/bash

# In container, check if xeyes works:
game_bot $ xeyes

# Install python dependencies
$ python3 -m pip install -r requirements.txt

# Close the container. Reuse with:
$ docker start -i game_bot

```


## Other notes
I had trouble using opencv's QT (imshow, ...) because of shared memory problems. It appears that MIT-SHM is an extension of the X server which speeds things up by sharing memory. We can disable MIT-SHM in Qt apps by using the env variable `QT_X11_NO_MITSHM=1`.


## TODO
Should I install python packages from Dockerfile or should I keep them in the python package. Probably best in python package.
