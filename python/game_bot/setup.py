from setuptools import setup

setup(name='game_bot',
      version='0.1',
      description='Game bot.',
      url='http://localhost',
      author='KV',
      author_email='kristjan.voje@gmail.com',
      license='MIT',
      packages=['game_bot'],
      install_requires=[], # todo
      zip_safe=False)
